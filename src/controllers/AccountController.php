<?php

namespace Swoe\Controllers;

use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Exceptions\UserNotFoundException;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\StatusInterface;
use Swoe\Models\Users\RolesInterface;
use Swoe\Models\Users\Users;

/**
 * Class AccountController
 * @package Swoe\Controllers
 * @RoutePrefix("/account")
 */
class AccountController extends ControllerBase
{
    public function initialize() {
        parent::initialize();

        $this->layoutStyles([
            'css/account/login.css'
        ]);
    }

    public function indexAction() {
        $this->dispatcher->forward([
            'action' => 'signin'
        ]);
    }

    /**
     * @Get("/signup", name="signup")
     */
    public function signupAction()
    {
        $this->view->title = 'Sign Up';

        $this->scripts([
            'js/account/signup.js'
        ]);
    }

    /**
     * @Post("/register", name="register")
     */
    public function registerAction()
    {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $email = $this->request->getPost('email');
        $email_confirmation = $this->request->getPost('emailConfirmation');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result->status = false;
            $result->errors[] = 'Email format is not valid';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($email !== $email_confirmation) {
            $result->status = false;
            $result->errors[] = 'Email and confirmation email do not match. Try again.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $password = $this->request->getPost('password');
        $password_length = mb_strlen($password);
        $password_confirmation = $this->request->getPost('passwordConfirmation');
        if ($password_length < Users::PASSWORD_MIN_LENGTH) {
            $result->status = false;
            $result->errors[] = 'Password must be at least 8 characters long.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($password !== $password_confirmation) {
            $result->status = false;
            $result->errors[] = 'Password and confirmation password do not match. Try again.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $terms = intval($this->request->getPost('terms', null, 0), 10);
        if ($terms !== 1) {
            $result->status = false;
            $result->errors[] = 'Must accept the <i>terms and conditions</i> to use this site.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $raw = new Users();
        $raw->email = $this->request->getPost('email');
        $raw->password = $this->security->hash($this->request->getPost('password'));
        $raw->token = str_replace(['/','$','.'], '', $this->security->hash($raw->email)); //token para la confirmación

        $name = explode(' ', $this->request->getPost('name', 'string', ''), 2);
        $raw->name = $name[0];
        if (count($name) > 1)
            $raw->surname = $name[1];

        $raw->company = $this->request->getPost('company', 'string');
        $raw->role = RolesInterface::REGISTERED_USER;
        $raw->status = StatusInterface::NOT_CONFIRMED;

        $raw->sanitize();
        if (Users::exists($raw->email)) {
            $result->status = false;
            $result->errors[] = 'A user with this email already exists! Try another one...';
            $this->response->setStatusCode(409);
            return $this->response->setJsonContent($result);
        }

        //Check if the insertion was successful
        if ($raw->save()) {
            $this->security->destroyToken();
            error_log('AccountController: user ' . $raw->name . ' created!');
            $this->sendConfirmation($raw);
            $result->data = [
                'id' => $raw->getId(),
                'url' => '/'
            ];
            return $this->response->setJsonContent($result);
        }

        error_log('user-save-notsaved');

        foreach ($raw->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        $result->status = false;
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    private function sendConfirmation(Users $user) {
        $message = \Swift_Message::newInstance(
            'SWoE - Account confirmation',
            'Hi! You\'re on step closer to be a part of SWoE. Please click on the link below to confirm your subscription. If you didn\t request this account, please ignore this message. www.swoe.eu/account/confirm/' . $user->token)
            ->setFrom('no-reply@swoe.eu', 'Semantic Web of Energy')
            ->setTo($user->email);

        $this->email->send($message);
    }

    /**
     * @Get("/confirm/{token}", name="confirm")
     * @param $token
     * @return bool|\Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function confirmAction($token) {
        try {
            $users = Users::findBy(['token' => $this->filter->sanitize($token, 'trim')]);
        } catch (DocumentNotFoundException $e) {
            $this->response->setStatusCode(403);
            $this->response->redirect('forbidden');
            return;
        }

        if (count($users) == 0) {
            $this->response->setStatusCode(404);
            $this->response->redirect('missing');
            return;
        }

        try {
            $user = Users::findById($users[0]->_id);
            $user->token = '';
            $user->status = StatusInterface::INACTIVE;
            $user->role = RolesInterface::REGISTERED_USER;

            if ($user->save()) {
                $this->registerSession($user, false);
                return $this->response->redirect('admin');
            }
        } catch (DocumentNotFoundException $e) {
            $this->response->setStatusCode(404);
            $this->response->redirect('missing');
            return;
        }

        $this->response->setStatusCode(404);
        $this->response->redirect('missing');
        return;
    }

    /**
     * @Get("/signin", name="signin")
     */
    public function signinAction()
    {
        $this->view->title = 'Sign In';

        $this->scripts([
            'js/account/signin.js'
        ]);
    }

    /**
     * @Post("/access", name="access")
     */
    public function accessAction()
    {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $email = $this->request->getPost('email', 'email');
        $password = $this->request->getPost('password');
        try {
            $user = Users::findActiveByEmail($email);
        } catch (UserNotFoundException $e) {
            $result->status = false;
            $result->errors[] = "The credentials used are not valid. Check both your e-mail and password.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkHash($password, $user->password)) {
            $result->status = false;
            $result->errors[] = "The credentials used are not valid. Check both your e-mail and password.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $remember = boolval($this->request->getPost('remember'));
        $this->registerSession($user, $remember);

        $result->data = ['url' => $this->url->get('admin')];
        return $this->response->setJsonContent($result);
    }

    /**
     * @Get("/forgot", name="forgot")
     */
    public function forgotAction()
    {

    }

    /**
     * @Post("/remember", name="remember")
     */
    public function rememberAction()
    {

    }

    /**
     * @Get("/logout", name="logout")
     */
    public function logoutAction() {
        $this->logger->log('ACCOUNT: Logout session ' . $this->session->getId());
        $this->view->disable();
        $this->session->set('auth', null);
        $this->session->destroy();
        $this->session->start();
        session_regenerate_id(true);
        $this->logger->log('ACCOUNT: Logout session ' . $this->session->getId());

        $this->response->setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
        $this->response->setHeader('Pragma', 'no-cache');
        $this->response->setHeader('Expires', 0);

        $this->response->redirect([
            'for' => 'homepage'
        ]);
    }

    /**
     * @param Users $user
     * @param bool $remember
     */
    private function registerSession(Users $user, $remember = false) {
        $contents = [
            'id'        => $user->getId(),
            'email'     => $user->email,
            'name'      => "{$user->name} {$user->surname}",
            'role'      => intval($user->role, 10),
            'remember'  => $remember
        ];
        $this->session->set('auth', $contents);
    }
}
