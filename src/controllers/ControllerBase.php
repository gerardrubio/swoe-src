<?php

namespace Swoe\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    const COLLECTION_ACTION_SCRIPTS = 'scripts';
    const COLLECTION_ACTION_STYLES  = 'styles';

    const COLLECTION_COMMON_SCRIPTS = 'common-scripts';
    const COLLECTION_COMMON_STYLES  = 'common-styles';

    const COLLECTION_IE             = 'ie';

    const COLLECTION_LAYOUT_SCRIPTS = 'layout-scripts';
    const COLLECTION_LAYOUT_STYLES  = 'layout-styles';

    private $viewsDir = '';

    protected function viewsDir($path) {
        $this->viewsDir = $path;
    }

    public function afterExecuteRoute()
    {
        $this->view->setViewsDir($this->view->getViewsDir() . $this->viewsDir);
    }

    public function initialize() {
        $this->styles([
            'vendor/semantic-ui/dist/semantic.min.css',
        ], static::COLLECTION_COMMON_STYLES);

        $this->scripts([
            'vendor/html5shiv/dist/html5shiv.min.js',
            'vendor/Respond/dest/respond.min.js'
        ], static::COLLECTION_IE);

        $this->scripts([
            'vendor/jquery/dist/jquery.min.js',
            'vendor/semantic-ui/dist/semantic.min.js',
            'js/common/modal.js',
            'js/common/sidebar.js'
        ], static::COLLECTION_COMMON_SCRIPTS);

        $this->view->description = 'Semantic Web of Energy, the easiest way to manage your energy';

        $this->assets->collection(static::COLLECTION_LAYOUT_SCRIPTS);
        $this->assets->collection(static::COLLECTION_LAYOUT_STYLES);

        $this->assets->collection(static::COLLECTION_ACTION_SCRIPTS);
        $this->assets->collection(static::COLLECTION_ACTION_STYLES);
    }

    /**
     * Añadir css al layout
     * @param array $stylesheets
     */
    protected function layoutStyles(array $stylesheets) {
        $this->styles($stylesheets, static::COLLECTION_LAYOUT_STYLES);
    }

    protected function layoutScripts(array $scripts) {
        $this->scripts($scripts, static::COLLECTION_LAYOUT_SCRIPTS);
    }

    /**
     * @param array $stylesheets
     * @param string $collection
     */
    protected function styles(array $stylesheets, $collection = self::COLLECTION_ACTION_STYLES) {
        foreach($stylesheets as $stylesheet) {
            $this->assets->collection($collection)
                ->addCss($stylesheet);
        }
    }

    /**
     * @param array $scripts
     * @param string $collection
     */
    protected function scripts(array $scripts, $collection = self::COLLECTION_ACTION_SCRIPTS) {
        foreach($scripts as $script) {
            $this->assets->collection($collection)
                ->addJs($script);
        }
    }

}
