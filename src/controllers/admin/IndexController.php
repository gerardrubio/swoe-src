<?php

namespace Swoe\Controllers\Admin;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Devices\Devices;
use Swoe\Models\Dimensions\Dimensions;
use Swoe\Models\FindableInterface;
use Swoe\Models\Readings\Readings;
use Swoe\Models\Users\RolesInterface;
use Swoe\Models\Users\Users;
use Swoe\Models\Views\Admin\Area;

/**
 * Class IndexController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin")
 * @Security(role="6")
 */
class IndexController extends AdminControllerBase
{
    public function initialize() {
        parent::initialize();

        $area = new Area();
        $area->color = 'black';
        $area->icon = 'dashboard';
        $area->id = 'dashboard';
        $area->label = 'Dashboard';

        $this->view->setVar('area', $area);
    }

    /**
     * @Get("/", name="dashboard")
     * @Cache()
     */
    public function indexAction()
    {
        $this->styles([
            'css/admin/index/index.css'
        ]);

        if ($this->view->auth['isAdmin']) {
            $this->view->users = Users::count();
            $this->view->devices = Devices::count();
            $this->view->dimensions = Dimensions::count();
            $this->view->readings = Readings::count();
        } else {
            $this->view->users = 0;
            $idField = '$id';
            $devices = Devices::findBy(
                [
                    'owner' => $this->view->auth['id']->$idField
                ],
                FindableInterface::ALL_DOCUMENTS,
                FindableInterface::SKIP_NONE,
                [
                    '_id' => 1
                ]
            );

            $this->view->devices = count($devices);
            $this->view->dimensions = 0;

            if ($this->view->devices > 0) {
                $ids = [];
                foreach ($devices as $index => $device) {
                    $ids[] = "this.device == '{$device->_id}'";
                }
                $idsQuery = implode(' || ', $ids);
                $this->view->readings = Readings::count([
                    'conditions' => [
                        '$where' => $idsQuery
                    ]
                ]);
            } else {
                $this->view->readings = 0;
            }

        }

        $this->view->title = 'Dashboard';
    }
}
