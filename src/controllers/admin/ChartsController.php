<?php

namespace Swoe\Controllers\Admin;

use MongoId;
use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Devices\Devices;
use Swoe\Models\Dimensions\Dimensions;
use Swoe\Models\FindableInterface;
use Swoe\Models\Protocols\Protocols;
use Swoe\Models\Readings\Readings;
use Swoe\Models\Readings\ReadingTranslator;
use Swoe\Models\StatusInterface;
use Swoe\Models\Views\Admin\Area;

/**
 * Class ChartsController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/charts")
 * @Security(role="6")
 */
class ChartsController extends AdminControllerBase
{
    public function initialize() {
        parent::initialize();

        $this->layoutScripts([
            'vendor/Flot/jquery.flot.js',
            'vendor/Flot/jquery.flot.time.js',
            //'vendor/flot.touch/jquery.flot.touch.js',
            'vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
            'vendor/flot.curvedlines/curvedLines.js',
            'vendor/flot-autoMarkings/jquery.flot.autoMarkings.js',
            'vendor/pickadate/lib/compressed/picker.js',
            'vendor/pickadate/lib/compressed/picker.date.js',
        ]);

        $this->layoutStyles([
            'vendor/pickadate/lib/compressed/themes/default.css',
            'vendor/pickadate/lib/compressed/themes/default.date.css',
        ]);

        $this->scripts([
            'vendor/ExplorerCanvas/excanvas.js',
            'vendor/pickadate/lib/compressed/legacy.js',
        ], static::COLLECTION_IE);

        $area = new Area();
        $area->color = 'green';
        $area->icon = 'line chart';
        $area->id = 'charts';
        $area->label = 'Charts';

        $this->view->setVar('area', $area);
    }

    /**
     * @Get("/")
     */
    public function indexAction()
    {
        $this->scripts([
            'js/admin/charts/index.js',
        ]);
        $this->styles(['css/admin/charts/index.css']);
        $this->view->title = 'Charts';

        if ($this->request->hasQuery('device')) {
            $device_id = $this->request->getQuery('device', 'string', '');
            $this->view->device = Devices::findById($device_id);
        } else {
            $this->view->device = '';
        }

        if ($this->request->hasQuery('dimension')) {
            $dimension_id = $this->request->getQuery('dimension', 'string', '');
            $this->view->dimension = Dimensions::findById($dimension_id);
        } else {
            $magnitude = Dimensions::getDefault();
            $this->view->dimension = $magnitude;
        }

        if ($this->request->hasQuery('from')) {
            $this->view->from = $this->request->getQuery('from', 'string', '');
        } else {
            $date = new \DateTime();
            $date->modify('first day of this month');
            $this->view->from = $date->format('Y-m-d');
        }

        if ($this->request->hasQuery('until')) {
            $this->view->until = $this->request->getQuery('until', 'string', '');
        } else {
            $date = new \DateTime();
            $date->modify('last day of this month');
            $this->view->until = $date->format('Y-m-d');
        }
    }

    /**
     * @Get('/readings/{deviceId}/{dimensionId}/{from}/{until}', name='chart-readings')
     * @param string $deviceId
     * @param string $dimensionId
     * @param mixed $from
     * @param mixed $until
     * @return Response|\Phalcon\Http\ResponseInterface
     * @Cache(kind='json', lifetime='3600')
     */
    public function readingsAction($deviceId, $dimensionId, $from = null, $until = null) {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isGet()) {
            $result->status = false;
            $result->errors[] = 'Wrong verb';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $queryFragments = [];
        if (!MongoId::isValid($dimensionId)) {
            $result->status = false;
            $result->errors[] = 'Bad dimension id';
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $dimension = Dimensions::findById($dimensionId);
        if ($dimension === null) {
            $result->status = false;
            $result->errors[] = "Dimension $dimensionId not found";
            $this->response->setStatusCode(404);
            return $this->response->setJsonContent($result);
        }

        $protocol = Protocols::findById($dimension->protocol);
        if ($protocol === null) {
            $result->status = false;
            $result->errors[] = "Protocol {$dimension->protocol} not found";
            $this->response->setStatusCode(404);
            return $this->response->setJsonContent($result);
        }

        $queryFragments[] = sprintf('this.protocol == "%s" && %s', $dimension->protocol, $dimension->matcher);

        if (!MongoId::isValid($deviceId)) {
            $result->status = false;
            $result->errors[] = 'Bad device id';
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $device = Devices::findById($deviceId);
        if ($device === null) {
            $result->status = false;
            $result->errors[] = "Device $deviceId not found";
            $this->response->setStatusCode(404);
            return $this->response->setJsonContent($result);
        }

        $queryFragments[] = sprintf('this.device == "%s"', $deviceId);

        if (is_null($from)) {
            $from = new \DateTime();
            $from->modify('first day of this month');
        } else {
            $from = date_create_from_format('Y-m-d', $from);
        }

        $queryFragments[] = sprintf('this.timestamp >= %d', $from->getTimestamp());

        if (is_null($from)) {
            $until = new \DateTime();
            $until->modify('last day of this month');
        } else {
            $until = date_create_from_format('Y-m-d', $until);
        }

        $queryFragments[] = sprintf('this.timestamp <= %d', $until->getTimestamp());

        $query = Readings::queryGenerator($queryFragments);
        $this->logger->log('CHARTS: Query: ' . $query);
        $readings = Readings::find([
            'conditions' => [
                '$where'    => $query
            ]
        ]);

        $result->data = ReadingTranslator::translate($readings, $dimension, $protocol);
        return $this->response->setJsonContent($result);
    }

}
