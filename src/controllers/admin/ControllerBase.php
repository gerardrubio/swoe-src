<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 12/08/2015
 * Time: 20:18
 */

namespace Swoe\Controllers\Admin;

use Swoe\Controllers\ControllerBase as RootControllerBase;
use Swoe\Models\Users\RolesInterface;

class ControllerBase extends RootControllerBase {
    public function initialize() {
        parent::initialize();

        $this->layoutStyles([
            'css/admin/general.css'
        ]);

        $this->viewsDir('admin/');
        $this->view->setLayout('index');
        $auth = $this->session->get('auth');
        $auth['isAdmin'] = ($auth['role'] & RolesInterface::ADMINISTRATOR) > 0;
        $this->view->auth = $auth;
    }
}
