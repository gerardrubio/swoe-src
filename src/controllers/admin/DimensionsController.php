<?php

namespace Swoe\Controllers\Admin;

use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Dimensions\Dimensions;
use Swoe\Models\FindableInterface;

/**
 * Class DimensionsController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/dimensions")
 * @Security(role="6")
 */
class DimensionsController extends AdminControllerBase
{
    /**
     * @Get("/search", name="search-dimensions")
     * @Cache(kind='json')
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function searchAction() {
        $this->view->disable();

        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $searchTerm = mb_strtolower(trim($this->request->getQuery('q', 'string')));
        $query = "this.name.toLocaleLowerCase().indexOf('$searchTerm') >= 0 || this.units.toLocaleLowerCase().indexOf('$searchTerm') >= 0";

        $result->data = Dimensions::findBy(['$where' => $query], FindableInterface::ALL_DOCUMENTS, FindableInterface::SKIP_NONE, [
            '_id'       => 1,
            'name'      => 1,
            'units'     => 1,
            'protocol'  => 1
        ]);

        return $this->response->setJsonContent($result);
    }

}
