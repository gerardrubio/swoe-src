<?php

namespace Swoe\Controllers\Admin;

use MongoId;
use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Ajax\DataTablesResponse;
use Swoe\Models\Dimensions\Dimensions;
use Swoe\Models\FindableInterface;
use Swoe\Models\Graph\GraphItem;
use Swoe\Models\PagingInterface;
use Swoe\Models\Protocols\Protocols;
use Swoe\Models\Readings\Readings;
use Swoe\Models\StatusInterface;
use Swoe\Models\Devices\Devices;
use Swoe\Models\Users\Users;
use Swoe\Models\Views\Admin\Area;
use Swoe\Models\Views\Admin\MenuItem;

/**
 * Class DevicesController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/devices")
 * @Security(role="6")
 */
class DevicesController extends AdminControllerBase
{
    public function initialize() {
        parent::initialize();

        $area = new Area();
        $area->color = 'olive';
        $area->icon = 'plug';
        $area->id = 'devices';
        $area->label = 'Devices';

        $this->view->setVar('area', $area);
    }

    /**
     * @Get("/")
     */
    public function indexAction()
    {
        $this->styles([
            'vendor/datatables/media/css/jquery.dataTables.min.css',
            'css/admin/devices/list.css',
        ]);
        $this->scripts([
            'vendor/datatables/media/js/jquery.dataTables.min.js',
            'js/admin/devices/list.js',
        ]);

        $listOption = new MenuItem();
        $listOption->active = true;
        $listOption->label = 'List';
        $listOption->url = 'admin/devices';

        $newOption = new MenuItem();
        $newOption->label = 'New';
        $newOption->url = 'admin/devices/new';

        $editOption = new MenuItem();
        $editOption->disabled = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->title = 'Devices';
        $this->view->devices = [];
    }

    /**
     * @Get("/list", name="list-devices")
     * @return mixed
     */
    public function listAction() {
        $this->view->disable();

        $result = new DataTablesResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $page = intval($this->request->getQuery('start', 'int', PagingInterface::DEFAULT_PAGE), 10);
        $length = intval($this->request->getQuery('length', 'int', PagingInterface::DEFAULT_PAGE_SIZE), 10);
        $draw = intval($this->request->getQuery('draw', 'int', 0), 10);
        $text = $this->request->getQuery('search', 'string', '');

        $result->draw = $draw;
        if ($this->view->auth['isAdmin']) {
            $result->recordsTotal = Devices::count();
            $result->recordsFiltered = Devices::count([
                'conditions' => [
                    '$where' => "this.name.toLocaleLowerCase().indexOf('{$text['value']}') >= 0"
                ]
            ]);
            $result->data = Devices::find([
                'conditions' => [
                    '$where' => "this.name.toLocaleLowerCase().indexOf('{$text['value']}') >= 0"
                ],
                'sort' => [
                    'name' => -1
                ],
                'limit' => $length,
                'skip' => $page,
                'fields' => [
                    'readings' => 0
                ]
            ]);
        } else {
            $result->recordsTotal = Devices::count([
                'conditions' => [
                    'owner' => $this->view->auth['id']
                ]
            ]);
            $result->recordsFiltered = Devices::count([
                'conditions' => [
                    '$where' => "this.owner == '{$this->view->auth['id']}' && this.name.toLocaleLowerCase().indexOf('{$text['value']}') >= 0"
                ]
            ]);
            $result->data = Devices::find([
                'conditions' => [
                    '$where' => "this.owner == '{$this->view->auth['id']}' && this.name.toLocaleLowerCase().indexOf('{$text['value']}') >= 0"
                ],
                'sort' => [
                    'name' => -1
                ],
                'limit' => $length,
                'skip' => $page,
                'fields' => [
                    'readings' => 0
                ]
            ]);
        }

        return $this->response->setJsonContent($result);
    }

    /**
     * @Get('/owner/{id:[0-9a-zA-Z]+}', name='devices-owned')
     */
    public function ownerAction($id) {
        $this->view->disable();

        $result = new DataTablesResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $page = intval($this->request->getQuery('start', 'int', PagingInterface::DEFAULT_PAGE), 10);
        $length = intval($this->request->getQuery('length', 'int', PagingInterface::DEFAULT_PAGE_SIZE), 10);
        $draw = intval($this->request->getQuery('draw', 'int', 0), 10);

        $result->draw = $draw;
        $result->recordsTotal = Devices::count();
        $result->recordsFiltered = Devices::count([
            'conditions' => [
                'owner' => $id
            ]
        ]);
        $result->data = Devices::find([
            'conditions' => [
                'owner' => $id
            ],
            'sort' => [
                'name' => -1
            ],
            'limit' => $length,
            'skip' => $page,
            'fields' => [
                'readings' => 0
            ]
        ]);

        return $this->response->setJsonContent($result);
    }

    /**
     * @Get("/new", name="new-device")
     */
    public function newAction()
    {
        $listOption = new MenuItem();
        $listOption->label = 'List';
        $listOption->url = 'admin/devices';

        $newOption = new MenuItem();
        $newOption->active = true;
        $newOption->label = 'New';
        $newOption->url = 'admin/devices/new';

        $editOption = new MenuItem();
        $editOption->disabled = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->owner = Users::findById($this->view->auth['id']);
        $this->view->protocol = Protocols::getDefault();

        $this->view->title = 'New device';
        $this->scripts([
            'https://maps.googleapis.com/maps/api/js',
            'vendor/openlayers3/build/ol.js',
            'js/admin/devices/new.js'
        ]);
    }

    /**
     * @Post('/save', name='save-device')
     */
    public function saveAction() {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            error_log('device-save-notpost');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            error_log('device-save-nottoken');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $raw = new Devices();
        $raw->name = $this->request->getPost('name', 'string', '');
        $raw->description = $this->request->getPost('description', 'string', '');
        $raw->address = $this->request->getPost('address', 'string', '');
        $coordinates = explode(',', $this->request->getPost('coordinates'));
        $raw->location['coordinates'] = [
            floatval($coordinates[0]),
            floatval($coordinates[1])
        ];
        $raw->owner = $this->request->getPost('owner', 'string', '');
        $raw->ip = $this->request->getPost('ip', 'string', '');
        $raw->status = StatusInterface::INACTIVE;

        $raw->sanitize();

        //Check if the insertion was successful
        if ($raw->save()) {
            $this->security->destroyToken();
            error_log('DevicesController: device ' . $raw->name . ' created!');
            $result->data = $raw->getId();
            return $this->response->setJsonContent($result);
        }

        error_log('device-save-notsaved');

        foreach ($raw->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        $result->status = false;
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    /**
     * @Get('/edit/{id:[0-9a-zA-Z]+}', name='edit-device')
     * @param $id
     */
    public function editAction($id) {
        $id = filter_var($id, FILTER_SANITIZE_STRING);

        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(404);
            return false;
        }

        $device = Devices::findById($id);
        if (!$device) {
            $this->response->setStatusCode(404);
            return false;
        }

        $listOption = new MenuItem();
        $listOption->label = 'List';
        $listOption->url = 'admin/devices';

        $newOption = new MenuItem();
        $newOption->label = 'New';
        $newOption->url = 'admin/devices/new';

        $editOption = new MenuItem();
        $editOption->active = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->title = 'Edit device';
        $this->view->device = $device;
        $this->view->owner = Users::findById($device->owner);
        $this->view->protocol = Protocols::findById($device->protocol);

        $this->scripts([
            'https://maps.googleapis.com/maps/api/js',
            'vendor/openlayers3/build/ol-debug.js',
            'js/admin/devices/edit.js'
        ]);
    }

    /**
     * @Post('/update/{id:[0-9a-zA-Z]+}', name='update-device')
     */
    public function updateAction($id) {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            error_log('device-update-notpost');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            error_log('device-update-nottoken');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(400);
            $result['errors'][] = '(isValid) Device id not valid. Try again.';
            return $this->response->setJsonContent($result);
        }

        $device = Devices::findById($id);
        if (!$device) {
            $this->response->setStatusCode(404);
            $result['errors'][] = '(findById) Device not found';
            return $this->response->setJsonContent($result);
        }

        $device->name = $this->request->getPost('name', 'string', '');
        $device->description = $this->request->getPost('description', 'string', '');
        $device->address = $this->request->getPost('address', 'string', '');
        $coordinates = explode(',', $this->request->getPost('coordinates'));
        $device->location['coordinates'] = [
            floatval($coordinates[0]),
            floatval($coordinates[1])
        ];
        $device->owner = $this->request->getPost('owner', 'string', '');
        $device->ip = $this->request->getPost('ip', 'string', '');

        $device->sanitize();

        //Check if the insertion was successful
        if ($device->save()) {
            $this->security->destroyToken();
            error_log('DevicesController: device ' . $device->name . ' updated!');
            $result->data = $id;
            return $this->response->setJsonContent($result);
        }

        error_log('device-update-notupdated');

        foreach ($device->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        $result->status = false;
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    /**
     * @param $id
     * @Route("/delete/{id:[0-9a-zA-Z]+}", methods={'DELETE','POST'}, name="delete-device")
     */
    public function deleteAction($id)
    {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            error_log('device-delete-notdelete');
            $this->response->setStatusCode(400);
            $result->status = false;
            $result->errors[] = '(isAjax) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isDelete()) {
            error_log('device-save-notput');
            $this->response->setStatusCode(405);
            $result->status = false;
            $result->errors[] = '(isPut) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }
        /*
        if (!$this->security->checkToken()) {
            error_log('user-save-nottoken');
            $this->response->setStatusCode(403);
            $result->status = false;
            $result->errors[] = '(checkToken) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }
*/
        $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(400);
            $result->status = false;
            $result->errors[] = '(isValid) Device id not valid. Try again.';
            return $this->response->setJsonContent($result);
        }

        $device = Devices::findById($id);
        if (!$device) {
            $this->response->setStatusCode(404);
            $result->status = false;
            $result->errors[] = '(findById) Device not found';
            return $this->response->setJsonContent($result);
        }

        if ($device->delete()) {
            $this->response->setStatusCode(200);
            $result->status = true;
            return $this->response->setJsonContent($result);
        }

        error_log('device-delete-notdeleted');

        $this->response->setStatusCode(409);
        $result->status = false;
        foreach ($device->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        return $this->response->setJsonContent($result);
    }

    /**
     * @Get("/search", name="search-devices")
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function searchAction() {
        $this->view->disable();

        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $searchTerm = mb_strtolower(trim($this->request->getQuery('q', 'string')));
        $query = "(this.name.toLocaleLowerCase().indexOf('$searchTerm') >= 0 || this.description.toLocaleLowerCase().indexOf('$searchTerm') >= 0)";

        if ($this->request->hasQuery('owner')) {
            $owner = $this->request->getQuery('owner', 'string', '');
        }

        if (!$this->view->auth['isAdmin']) {
            $idField = '$id';
            $owner = $this->view->auth['id']->$idField;
        }

        if (!empty($owner))
            $query .= " && this.owner == '$owner'";

        $result->data = Devices::findBy(['$where' => $query], FindableInterface::ALL_DOCUMENTS, FindableInterface::SKIP_NONE, [
            '_id'       => 1,
            'name'      => 1,
            'protocol'  => 1
        ]);

        return $this->response->setJsonContent($result);
    }



    /**
     * @Get('/test/{longitude}/{latitude}/{radius}', name='test')
     * @Cache(kind='json', lifetime=30)
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function testAction($latitude, $longitude, $radius) {

        $devices = Devices::findAroundPoint(floatval($latitude), floatval($longitude), floatval($radius), [
            'location.coordinates' => 1
        ]);
        $results = [];
        $dimensions = Dimensions::getDefault();
        $dimensionMatcher = $dimensions->aggregationQuery();
        foreach ($devices  as $device) {

            $graph = new GraphItem();

            $matcher = [
                'device' => $device->_id,
                'protocol' => $dimensions->protocol,
            ];
            $matcher = array_merge($matcher, $dimensionMatcher);

            $readings = Readings::aggregate([
                [
                    '$match' => $matcher,
                ],
                [
                    '$group' => [
                        '_id' => '$device',
                        'avg' => [
                            '$avg' => '$data.f'
                        ]
                    ]
                ]
            ]);

            if (!isset($readings['result'][0]))
                continue;
            $graph->id = $device->_id;
            $graph->location = $device->location;
            $graph->value = $readings['result'][0]['avg'];
            $results[] = $graph;
        }

        return $this->response->setJsonContent($results);
    }

    /**
     * @Get('/info/{deviceId}', name='device-info')
     * @Cache(kind='json')
     * @param $deviceId
     * @return Response|\Phalcon\Http\ResponseInterface|void
     */
    public function infoAction($deviceId) {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!MongoId::isValid($deviceId)) {
            $result->status = false;
            $this->response->setStatusCode(400);
            $result->errors[] = 'Invalid device id';
            $this->response->setJsonContent($result);
            return;
        }

        try {
            $device = Devices::findById($deviceId);
        } catch (DocumentNotFoundException $e) {
            $result->status = false;
            $this->response->setStatusCode(404);
            $result->errors[] = 'Device not found';
            $this->response->setJsonContent($result);
            return;
        }

        try {
            $owner = Users::findById($device->owner);
        } catch (DocumentNotFoundException $e) {
            $result->status = false;
            $this->response->setStatusCode(404);
            $result->errors[] = 'Device owner not found';
            $this->response->setJsonContent($result);
        }

        $result->data['device'] = $device;
        $result->data['owner'] = $owner;
        return $this->response->setJsonContent($result);
    }
}
