<?php

namespace Swoe\Controllers\Admin;

use MongoId;
use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Devices\Devices;
use Swoe\Models\Users\Users;
use Swoe\Models\Views\Admin\Area;
use Swoe\Models\Views\Admin\MenuItem;

/**
 * Class MapsController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/maps")
 * @Security(role="6")
 */
class MapsController extends AdminControllerBase
{
    const EARTH_RADIUS  = 6371.0072;
    const KM_TO_M       = 0.001;

    public function initialize() {
        parent::initialize();

        $this->layoutScripts([
            'vendor/openlayers3/build/ol.js',
        ]);

        $area = new Area();
        $area->color = 'teal';
        $area->icon = 'world';
        $area->id = 'maps';
        $area->label = 'Maps';

        $this->view->setVar('area', $area);
    }

    /**
     * @Get("/")
     */
    public function indexAction()
    {
        $this->scripts([
            'js/admin/maps/index.js',
        ]);

        $this->styles([
            'css/admin/maps/index.css'
        ]);

        $this->view->title = 'Maps';

        if ($this->request->hasQuery('device')) {
            $device_id = $this->request->getQuery('device', 'string', '');
            $this->view->device = Devices::findById($device_id);
        } else {
            $this->view->device = '';
        }

        if ($this->request->hasQuery('owner')) {
            $user_id = $this->request->getQuery('owner', 'string', '');
            $this->view->owner = Users::findById($user_id);
        } else {
            $this->view->owner = '';
        }

        $listOption = new MenuItem();
        $listOption->active = true;
        $listOption->label = 'Map';
        $listOption->url = 'admin/maps';

        $discoverOption = new MenuItem();
        $discoverOption->label = 'Discover';
        $discoverOption->url = 'admin/maps/discover';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $discoverOption,
        ]);
    }

    /**
     * @Get('/locate[/]?{ownerId}[/]?{deviceId}', name='map-locate')
     * @param string $ownerId
     * @param string $deviceId
     * @Cache(kind='json')
     * @return mixed|Response|\Phalcon\Http\ResponseInterface
     */
    public function locateAction($ownerId = '', $deviceId = '') {
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            $this->response->setJsonContent($result);
            return;
        }

        if (!$this->request->isGet()) {
            $result->status = false;
            $result->errors[] = 'Wrong verb';
            $this->response->setStatusCode(400);
            $this->response->setJsonContent($result);
            return;
        }

        $queryFragments = [];
        if (MongoId::isValid($ownerId)) {
            $queryFragments[] = sprintf('this.owner == "%s"', $ownerId);
        }

        if (MongoId::isValid($deviceId)) {
            $queryFragments[] = sprintf('this._id == "%s"', $deviceId);
        }

        $query = Devices::queryGenerator($queryFragments);
        $this->logger->log('MAPS: Query: ' . $query);
        if (count($queryFragments) > 0) {
            $devices = Devices::find([
                'conditions' => [
                    '$where' => $query
                ],
                'fields' => [
                    'location' => 1
                ]
            ]);
        } else {
            $devices = Devices::find([
                'fields' => [
                    'location' => 1
                ]
            ]);
        }

        $result->data['devices'] = $devices;
        return $this->response->setJsonContent($result);
    }

    /**
     * @Get('/discover', name='map-discover')
     * @Cache()
     */
    public function discoverAction() {
        $this->scripts([
            'https://maps.googleapis.com/maps/api/js',
            'js/admin/maps/discover.js',
        ]);

        $this->styles([
            'css/admin/maps/discover.css'
        ]);

        $this->view->title = 'Maps';

        $listOption = new MenuItem();
        $listOption->label = 'Map';
        $listOption->url = 'admin/maps';

        $discoverOption = new MenuItem();
        $discoverOption->active = true;
        $discoverOption->label = 'Discover';
        $discoverOption->url = 'admin/maps/discover';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $discoverOption,
        ]);
    }

    /**
     * @Get('/reveal/{latitude}/{longitude}[/]?{radius}', name='map-reveal')
     * @param float $latitude
     * @param float $longitude
     * @param int $radius
     * @Cache(kind='json')
     * @return mixed|Response|\Phalcon\Http\ResponseInterface
     */
    public function revealAction($latitude, $longitude, $radius = 1000) {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            $this->response->setJsonContent($result);
            return;
        }

        if (!$this->request->isGet()) {
            $result->status = false;
            $result->errors[] = 'Wrong verb';
            $this->response->setStatusCode(400);
            $this->response->setJsonContent($result);
            return;
        }

        $latitude = floatval($latitude);
        $longitude = floatval($longitude);
        $radius = intval($radius, 10) * static::KM_TO_M;

        $owner = $this->view->auth['id'];
        $this->logger->log('Find devices around point for user ' . $owner);
        $devices = Devices::findAroundPoint($latitude, $longitude, $radius / static::EARTH_RADIUS, [
            'location' => 1
        ], $owner);
        $result->data['devices'] = $devices;

        return $this->response->setJsonContent($result);
    }

}
