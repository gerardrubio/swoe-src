<?php

namespace Swoe\Controllers\Admin;

use MongoId;
use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Ajax\DataTablesResponse;
use Swoe\Models\FindableInterface;
use Swoe\Models\PagingInterface;
use Swoe\Models\StatusInterface;
use Swoe\Models\Users\RolesInterface;
use Swoe\Models\Users\Users;
use Swoe\Models\Views\Admin\Area;
use Swoe\Models\Views\Admin\MenuItem;

/**
 * Class UsersController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/users")
 * @Security(role="4")
 */
class UsersController extends AdminControllerBase
{
    public function initialize() {
        parent::initialize();

        $area = new Area();
        $area->color = 'orange';
        $area->icon = 'user';
        $area->id = 'users';
        $area->label = 'Users';

        $this->view->setVar('area', $area);
    }

    /**
     * @Get("/")
     */
    public function indexAction()
    {
        $this->styles([
            'vendor/datatables/media/css/jquery.dataTables.min.css',
            'css/admin/users/list.css',
        ]);
        $this->scripts([
            'vendor/datatables/media/js/jquery.dataTables.min.js',
            'js/admin/users/list.js',
        ]);

        $listOption = new MenuItem();
        $listOption->active = true;
        $listOption->label = 'List';
        $listOption->url = 'admin/users';

        $newOption = new MenuItem();
        $newOption->label = 'New';
        $newOption->url = 'admin/users/new';

        $editOption = new MenuItem();
        $editOption->disabled = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->title = 'Users';
        $this->view->users = [];
    }

    /**
     * @Get("/list", name="list-users")
     * @return mixed
     */
    public function listAction() {
        $this->view->disable();

        $result = new DataTablesResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $page = intval($this->request->getQuery('start', 'int', PagingInterface::DEFAULT_PAGE), 10);
        $length = intval($this->request->getQuery('length', 'int', PagingInterface::DEFAULT_PAGE_SIZE), 10);
        $draw = intval($this->request->getQuery('draw', 'int', 0), 10);

        $result->draw = $draw;
        $text = $this->request->getQuery('search', 'string', '');

        $result->draw = $draw;
        $result->recordsTotal = Users::count();
        $query = "this.name.toLocaleLowerCase().indexOf('{$text['value']}') >= 0 || " .
            "this.surname.toLocaleLowerCase().indexOf('{$text['value']}') >= 0 || " .
            "this.company.toLocaleLowerCase().indexOf('{$text['value']}') >= 0 || " .
            "this.email.toLocaleLowerCase().indexOf('{$text['value']}') >= 0";

        $result->recordsFiltered = Users::count([
            'conditions' => [
                '$where' => $query
            ]
        ]);

        $result->data = Users::find([
            'conditions' => [
                '$where' => $query
            ],
            'sort' => [
                'name' => -1
            ],
            'limit' => $length,
            'skip' => $page,
            'fields' => [
                'readings' => 0
            ]
        ]);

        return $this->response->setJsonContent($result);
    }

    /**
     * @Get("/search", name="search-users")
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function searchAction() {
        $this->view->disable();

        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $searchTerm = mb_strtolower(trim($this->request->getQuery('q', 'string')));
        $query = "this.name.toLocaleLowerCase().indexOf('$searchTerm') >= 0 || this.surname.toLocaleLowerCase().indexOf('$searchTerm') >= 0 || this.company.toLocaleLowerCase().indexOf('$searchTerm') >= 0";

        $result->data = Users::findBy(['$where' => $query], FindableInterface::ALL_DOCUMENTS, FindableInterface::SKIP_NONE, [
            '_id'       => 1,
            'name'      => 1,
            'surname'   => 1,
            'company'   => 1
        ]);

        return $this->response->setJsonContent($result);
    }

    /**
     * @Get("/new", name="new-user")
     */
    public function newAction()
    {
        $listOption = new MenuItem();
        $listOption->label = 'List';
        $listOption->url = 'admin/users';

        $newOption = new MenuItem();
        $newOption->active = true;
        $newOption->label = 'New';
        $newOption->url = 'admin/users/new';

        $editOption = new MenuItem();
        $editOption->disabled = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->title = 'New user';
        $this->view->roles = RolesInterface::AsList;
        $this->scripts([
            'js/admin/users/new.js'
        ]);
    }

    /**
     * @param $id
     * @Get("/edit/{id:[0-9a-zA-Z]+}", name="edit-user")
     */
    public function editAction($id)
    {
        $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(404);
            return false;
        }

        $user = Users::findById($id);
        if (!$user) {
            $this->response->setStatusCode(404);
            $this->flash->error("User not found.");
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return false;
        }

        $listOption = new MenuItem();
        $listOption->label = 'List';
        $listOption->url = 'admin/users';

        $newOption = new MenuItem();
        $newOption->label = 'New';
        $newOption->url = 'admin/users/new';

        $editOption = new MenuItem();
        $editOption->disabled = true;
        $editOption->active = true;
        $editOption->label = 'Edit';

        $this->view->setVar('sectionMenu', [
            $listOption,
            $newOption,
            $editOption,
        ]);

        $this->view->title = 'Edit user';
        $this->view->roles = RolesInterface::AsList;
        $this->view->user = $user;
        $this->scripts([
            'vendor/datatables/media/js/jquery.dataTables.min.js',
            'js/admin/users/edit.js'
        ]);

        $this->styles([
            'vendor/datatables/media/css/jquery.dataTables.min.css'
        ]);
    }

    /**
     * @Post("/save", name="save-user")
     */
    public function saveAction()
    {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            error_log('user-save-notpost');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            error_log('user-save-nottoken');
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $email = $this->request->getPost('email');
        $email_confirmation = $this->request->getPost('emailConfirmation');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result->status = false;
            $result->errors[] = 'Email format is not valid';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($email !== $email_confirmation) {
            $result->status = false;
            $result->errors[] = 'Email and confirmation email do not match. Try again.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $password = $this->request->getPost('password');
        $password_length = mb_strlen($password);
        $password_confirmation = $this->request->getPost('passwordConfirmation');
        if ($password_length < Users::PASSWORD_MIN_LENGTH) {
            $result->status = false;
            $result->errors[] = 'Password must be at least 8 characters long.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($password !== $password_confirmation) {
            $result->status = false;
            $result->errors[] = 'Password and confirmation password do not match. Try again.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $raw = new Users();
        $raw->email = $this->request->getPost('email');
        $raw->password = $this->security->hash($this->request->getPost('password'));
        $raw->name = $this->request->getPost('name');
        $raw->surname = $this->request->getPost('surname');
        $raw->company = $this->request->getPost('company');
        $raw->role = intval($this->request->getPost('role'), 10);
        $raw->status = StatusInterface::INACTIVE;

        $raw->sanitize();
        if (Users::exists($raw->email)) {
            $result->status = false;
            $result->errors[] = 'A user with this email already exists! Try another one...';
            $this->response->setStatusCode(409);
            return $this->response->setJsonContent($result);
        }

        //Check if the insertion was successful
        if ($raw->save()) {
            $this->security->destroyToken();
            error_log('UsersController: user ' . $raw->name . ' created!');
            $result->data = $raw->getId();
            return $this->response->setJsonContent($result);
        }

        error_log('user-save-notsaved');

        foreach ($raw->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        $result->status = false;
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    /**
     * @Post("/update/{id:[0-9a-zA-Z]+}", name="update-user")
     */
    public function updateAction($id) {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request - Not ajax';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            error_log('user-update-notpost');
            $result->status = false;
            $result->errors[] = "Bad request! Try again. - Not post";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            error_log('user-update-nottoken');
            $result->status = false;
            $result->errors[] = "Bad request! Try again. - Bad token";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(400);
            $result['errors'][] = '(isValid) User id not valid. Try again.';
            return $this->response->setJsonContent($result);
        }

        $user = Users::findById($id);
        if (!$user) {
            $this->response->setStatusCode(404);
            $result['errors'][] = '(findById) User not found';
            return $this->response->setJsonContent($result);
        }

        $email = $this->request->getPost('email');
        $email_confirmation = $this->request->getPost('emailConfirmation');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result->status = false;
            $result->errors[] = 'Email format is not valid';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($email !== $email_confirmation) {
            $result->status = false;
            $result->errors[] = 'Email and confirmation email do not match. Try again.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $password = $this->request->getPost('password');
        $password_length = mb_strlen($password);
        $password_confirmation = $this->request->getPost('password-confirmation');
        $password_confirmation_length = mb_strlen($password_confirmation);
        if ($password_length > 0 && $password_confirmation_length > 0) {
            if ($password_length < Users::PASSWORD_MIN_LENGTH) {
                $result->status = false;
                $result->errors[] = 'Password must be at least 8 characters long.';
                $this->response->setStatusCode(400);
                return $this->response->setJsonContent($result);
            }

            if ($password !== $password_confirmation) {
                $result->status = false;
                $result->errors[] = 'Password and confirmation password do not match. Try again.';
                $this->response->setStatusCode(400);
                return $this->response->setJsonContent($result);
            }
        }

        $user->email = $this->request->getPost('email');
        if ($password_length >= Users::PASSWORD_MIN_LENGTH)
            $user->password = $this->security->hash($password);

        $user->name = $this->request->getPost('name');
        $user->surname = $this->request->getPost('surname');
        $user->company = $this->request->getPost('company');
        $user->role = intval($this->request->getPost('role'), 10);

        $user->sanitize();

        //Check if the insertion was successful
        if ($user->save()) {
            $this->security->destroyToken();
            error_log('UsersController: user ' . $user->name . ' updated!');
            $result->data = $id;
            return $this->response->setJsonContent($result);
        }

        error_log('user-save-notsaved');

        foreach ($user->getMessages() as $message) {
            $result->errors[] = $message->getMessage();
        }

        $result->status = false;
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    /**
     * @param $id
     * @Delete("/delete/{id}", name="delete-user")
     */
    public function deleteAction($id)
    {
        $result = [
            'status' => false,
            'errors' => [],
            'data' => []
        ];

        if (!$this->request->isAjax()) {
            error_log('user-delete-notdelete');
            $this->response->setStatusCode(400);
            $result['errors'][] = '(isAjax) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isDelete()) {
            error_log('user-save-notput');
            $this->response->setStatusCode(405);
            $result['errors'][] = '(isPut) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }
        /*
        if (!$this->security->checkToken()) {
            error_log('user-save-nottoken');
            $this->response->setStatusCode(403);
            $result['errors'][] = '(checkToken) Bad request! Try again.';
            return $this->response->setJsonContent($result);
        }
*/
        $id = filter_var(trim($id), FILTER_SANITIZE_STRING);
        if (!MongoId::isValid($id)) {
            $this->response->setStatusCode(400);
            $result['errors'][] = '(isValid) User id not valid. Try again.';
            return $this->response->setJsonContent($result);
        }

        $user = Users::findById($id);
        if (!$user) {
            $this->response->setStatusCode(404);
            $result['errors'][] = '(findById) User not found';
            return $this->response->setJsonContent($result);
        }

        if ($user->delete()) {
            $this->response->setStatusCode(200);
            $result['status'] = true;
            return $this->response->setJsonContent($result);
        }

        error_log('user-delete-notdeleted');

        $this->response->setStatusCode(409);
        $errors = array();
        foreach ($user->getMessages() as $message) {
            $errors[] = $message->getMessage();
        }

        $result['errors'][] = $errors;
        return $this->response->setJsonContent($result);
    }

    /**
     * @param $userId
     * @param $deviceId
     * @Post("/assign", name="assign-device")
     */
    public function assignDevice($userId, $deviceId) {
        if (!$this->request->isPost()) {
            $this->response->setStatusCode(405);
            $this->response->setJsonContent(['errors' => ['Method Not Allowed']]);
            return;
        }

        if ($this->security->checkToken()) {
            $this->response->setStatusCode(403);
            $this->response->setJsonContent(['errors' => ['Forbidden']]);
            return;
        }

        try {
            $user = Users::findById($userId);
        } catch (\MongoException $e) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['errors' => ['User not found']]);
            return;
        }

        if ($user === false) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['errors' => ['User not found']]);
            return;
        }

        $user->registerDevice($deviceId);
        $this->response->setStatusCode(200);
        $this->response->setJsonContent(['status' => 'OK']);
    }

    /**
     * @param $userId
     * @param $deviceId
     * @Delete("/revoke", name="revoke-device")
     */
    public function revokeDevice($userId, $deviceId) {
        if (!$this->request->isDelete()) {
            $this->response->setStatusCode(405);
            $this->response->setJsonContent(['errors' => ['Method Not Allowed']]);
            return;
        }

        if ($this->security->checkToken()) {
            $this->response->setStatusCode(403);
            $this->response->setJsonContent(['errors' => ['Forbidden']]);
            return;
        }

        try {
            $user = Users::findById($userId);
        } catch (\MongoException $e) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['errors' => ['User not found']]);
            return;
        }

        if ($user === false) {
            $this->response->setStatusCode(404);
            $this->response->setJsonContent(['errors' => ['User not found']]);
            return;
        }

        $user->deregisterDevice($deviceId);
        $this->response->setStatusCode(200);
        $this->response->setJsonContent(['status' => 'OK']);
    }
}
