<?php

namespace Swoe\Controllers\Admin;

use Phalcon\Http\Response;
use Swoe\Controllers\Admin\ControllerBase as AdminControllerBase;
use Swoe\Models\Ajax\AjaxResponse;
use Swoe\Models\Protocols\Protocols;
use Swoe\Models\FindableInterface;

/**
 * Class ProtocolsController
 * @package Swoe\Controllers\Admin
 * @RoutePrefix("/admin/protocols")
 * @Security(role="6")
 */
class ProtocolsController extends AdminControllerBase
{
    /**
     * @Get("/search", name="search-protocols")
     * @Cache(kind='json')
     * @return Response|\Phalcon\Http\ResponseInterface
     */
    public function searchAction() {
        $this->view->disable();

        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $searchTerm = mb_strtolower(trim($this->request->getQuery('q', 'string')));
        $query = "this.name.toLocaleLowerCase().indexOf('$searchTerm') >= 0";

        $result->data = Protocols::findBy(['$where' => $query], FindableInterface::ALL_DOCUMENTS, FindableInterface::SKIP_NONE, [
            '_id'       => 1,
            'name'      => 1
        ]);

        return $this->response->setJsonContent($result);
    }

}
