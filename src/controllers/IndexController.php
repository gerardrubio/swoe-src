<?php

namespace Swoe\Controllers;
use Swoe\Models\Ajax\AjaxResponse;

/**
 * Class IndexController
 * @package Swoe\Controllers
 * @RoutePrefix("/")
 */
class IndexController extends ControllerBase
{
    public function initialize() {
        parent::initialize();

        $this->layoutStyles([
            '//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic',
            'css/index-common.css'
        ]);
    }

    /**
     * @Get("/", name='homepage')
     * @Cache()
     */
    public function indexAction()
    {
        $this->view->title = 'Home';
        $this->view->menu_active = 'home';
        $this->styles([
            'css/home.css'
        ]);

        $this->scripts([
            'vendor/openlayers3/build/ol.js',
            'js/index.js'
        ]);
    }

    /**
     * @Get("about", name="about")
     * @Cache()
     */
    public function aboutAction()
    {
        $this->view->title = 'About';
        $this->view->menu_active = 'about';
        $this->view->description = 'Semantic Web of Energy - What is it?';
        $this->styles([
            'css/about.css'
        ]);
    }

    /**
     * @Get("contact", name="contact")
     */
    public function contactAction()
    {
        $this->view->title = 'Contact Us';
        $this->view->menu_active = 'contact';
        $this->view->description = 'Semantic Web of Energy - Need more information?';
        $this->styles([
            'css/contact.css'
        ]);

        $this->scripts([
            'https://www.google.com/recaptcha/api.js',
            'js/contact.js'
        ]);
    }

    /**
     * @Post("contact-submit", name="contact-submit")
     */
    public function contactSubmitAction()
    {
        $this->view->disable();
        $result = new AjaxResponse();

        if (!$this->request->isAjax()) {
            $result->status = false;
            $result->errors[] = 'Bad request';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (!$this->request->isPost()) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(405);
            return $this->response->setJsonContent($result);
        }

        if (!$this->security->checkToken(null, null, false)) {
            $result->status = false;
            $result->errors[] = "Bad request! Try again.";
            $this->response->setStatusCode(403);
            return $this->response->setJsonContent($result);
        }

        $name = $this->request->getPost('name', 'string', '');
        if (empty($name)) {
            $result->status = false;
            $result->errors[] = 'Name is required';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $email = $this->request->getPost('email');
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result->status = false;
            $result->errors[] = 'Email format is not valid';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $content = $this->request->getPost('content', 'string', '');
        if (empty($content)) {
            $result->status = false;
            $result->errors[] = 'Message is required';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if (mb_strtolower($content) > 1000) {
            $result->status = false;
            $result->errors[] = 'Message must be 1000 characters at most';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $terms = intval($this->request->getPost('terms', null, 0), 10);
        if ($terms !== 1) {
            $result->status = false;
            $result->errors[] = 'Must accept the <i>terms and conditions</i> to use this site.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        $recaptcha = $this->request->getPost('g-recaptcha-response', 'string', '');
        if (!$this->validateRecaptcha($recaptcha)) {
            $result->status = false;
            $result->errors[] = 'Are you a robot? Please make sure to complete the captcha correctly.';
            $this->response->setStatusCode(400);
            return $this->response->setJsonContent($result);
        }

        if ($this->sendConfirmation($name, $email, $content)) {
            $this->security->destroyToken();
            return $this->response->setJsonContent($result);
        }

        $result->status = false;
        $result->errors[] = 'The message could not be sent';
        $this->response->setStatusCode(409);
        return $this->response->setJsonContent($result);
    }

    private function validateRecaptcha($response) {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $secret = '6LcLDwwTAAAAAKYM3Qf7d3jMiGRQy8nR93dYSVlh';
        $remoteip =  $this->request->getClientAddress();

        $ch = curl_init($url);
        $options = [
            CURLOPT_POSTFIELDS => [
                'remoteip' => $remoteip,
                'response' => $response,
                'secret' => $secret,
            ],
            CURLOPT_RETURNTRANSFER => true,
        ];

        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        $json_data = json_decode($data);

        return $json_data && $json_data->success;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string content
     */
    private function sendConfirmation($name, $email, $content) {
        $message = \Swift_Message::newInstance(
            'SWoE - Contact',
            "$name ($email) sent the following message\n\n$content")
            ->setFrom('no-reply@swoe.eu', 'Semantic Web of Energy')
            ->setTo('contact@swoe.eu', 'Contact')
            ->setCc($email, $name);

        return $this->email->send($message);
    }

    /**
     * @Get("404", name="missing")
     */
    public function missingAction() {
        $this->response->setStatusCode(404);

        if ($this->request->isAjax()) {
            $this->view->disable();
            $result = new AjaxResponse();
            $result->status = false;
            $result->errors[] = 'Action not found';
            return $this->response->setJsonContent($result);
        }

        $this->view->title = '404';

        $this->styles([
            'css/404.css'
        ]);
    }

    /**
     * @Get("forbidden", name="forbidden")
     */
    public function forbiddenAction() {
        $this->response->setStatusCode(403);

        if ($this->request->isAjax()) {
            $this->view->disable();
            $result = new AjaxResponse();
            $result->status = false;
            $result->errors[] = 'Action forbidden';
            return $this->response->setJsonContent($result);
        }

        $this->view->title = 'Forbidden';

        $this->styles([
            'css/forbidden.css'
        ]);
    }
}
