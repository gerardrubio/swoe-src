<p class="login-box-msg">Good job! You'll receive an email shortly to confirm your account.</p>
<p class="login-box-msg">Meanwhile, you may <a href="{{ url() }}">return to the start page</a> or learn more about SWoE.</p>
