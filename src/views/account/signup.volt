{{ form('account/register', 'method' : 'post', 'class': 'ui large form') }}
<div class="ui stacked segment">
    <h3>Register a new membership</h3>
    <div class="field">
        <div class="ui left icon input">
            <i class="user icon"></i>
            {{ text_field('name', 'placeholder': 'Full name', 'required': 'required') }}
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <i class="suitcase icon"></i>
            {{ text_field('company', 'placeholder': 'Company') }}
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <i class="mail icon"></i>
            {{ email_field('email', 'placeholder': 'E-mail address', 'required': 'required') }}
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <i class="mail icon"></i>
            {{ email_field('emailConfirmation', 'placeholder': 'Confirm your e-mail address', 'required': 'required') }}
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <i class="lock icon"></i>
            {{ password_field('password', 'placeholder': 'Password', 'required': 'required') }}
        </div>
    </div>
    <div class="field">
        <div class="ui left icon input">
            <i class="lock icon"></i>
            {{ password_field('passwordConfirmation', 'placeholder': 'Confirm your password', 'required': 'required') }}
        </div>
    </div>
    <div class="field">
        <div class="ui toggle checkbox">
            {{ check_field('terms', 'value': 1, 'required': 'required') }}
            <label for="terms"> I agree to the <a href="#">terms and conditions</a></label>
        </div>
    </div>
    {{ submit_button('Register', 'class': 'ui fluid large positive submit button') }}
</div>

<div class="ui error message"></div>

{{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
{{ endform() }}

<div class="ui message">
    {{ link_to('account/signin', 'I already have an account') }}
    {{ link_to('account/forgot', 'I forgot my password') }}
</div>
