{% extends "layouts/account-form.volt" %}

{% block title %}Forgot your password?{% endblock %}

{% block body_classes %}login-page{% endblock %}

{% block content %}
    {{ super() }}
    <p class="login-box-msg">Restore your credentials</p>

    {{ form('account/remember', 'method' : 'post') }}
    <div class="form-group has-feedback">
        {{ text_field('email', 'class': 'form-control', 'placeholder': 'Email') }}
        <span class="form-control-feedback"><i class="fa fa-envelope fa-fw"></i></span>
    </div>
    <div class="row">
        <div class="col-xs-4 pull-right">
            {{ submit_button('Recover', 'class': 'btn btn-primary btn-block btn-flat') }}
        </div>
        <!-- /.col -->
    </div>
    {{ endform() }}

    {# include "partials/social-auth.volt" #}
    {{ link_to('account/signup', 'Register a new membership', 'class': 'text-center') }}
    {{ link_to('account/signin', 'I remember my password') }}
{% endblock %}
