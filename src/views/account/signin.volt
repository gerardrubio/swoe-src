{{ form('account/access', 'method' : 'post', 'class': 'ui large form') }}
    <div class="ui stacked segment">
        <h3>Sign in to start your session</h3>
        <div class="field">
            <div class="ui left icon input">
                <i class="mail icon"></i>
                {{ email_field('email', 'placeholder': 'E-mail address', 'required': 'required') }}
            </div>
        </div>
        <div class="field">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                {{ password_field('password', 'placeholder': 'Password', 'required': 'required') }}
            </div>
        </div>
        <div class="field">
            <div class="ui toggle checkbox">
                {{ check_field('remember', 'class': 'hidden', 'value':1) }}
                <label>Remember me</label>
            </div>
        </div>
        {{ submit_button('Sign In', 'class': 'ui fluid large positive submit button') }}
    </div>

    <div class="ui error message"></div>

    {{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
{{ endform() }}

<div class="ui message">
    {{ link_to('account/signup', 'Register a new membership') }}
    {{ link_to('account/forgot', 'I forgot my password') }}
</div>
