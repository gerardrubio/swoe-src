{% extends 'templates/base.volt' %}

{% block body %}
    <!-- Sidebar Menu -->
    <div class="ui vertical inverted sidebar menu">
        <a class="{{ menu_active|default('') == 'home' ? 'active' : '' }} item" href="{{ url() }}">Home</a>
        <a class="{{ menu_active|default('') == 'about' ? 'active' : '' }} item" href="{{ url('about') }}">About</a>
        <a class="{{ menu_active|default('') == 'contact' ? 'active' : '' }} item" href="{{ url('contact') }}">Contact</a>
        <a class="item" href="{{ url('account/signin') }}">Log in</a>
        <a class="item" href="{{ url('account/signup') }}">Sign up</a>
    </div>
    <div class="pusher">

        <div class="ui container">
            <div class="ui large secondary pointing menu public-top-menu">
                <a class="toc item">
                    <i class="sidebar icon"></i>
                </a>
                <a href="{{ url() }}" class="{{ menu_active|default('') == 'home' ? 'active' : '' }} item">Home</a>
                <a href="{{ url('about') }}" class="{{ menu_active|default('') == 'about' ? 'active' : '' }} item">About</a>
                <a href="{{ url('contact') }}" class="{{ menu_active|default('') == 'contact' ? 'active' : '' }} item">Contact</a>

                <div class="right item">
                    <a class="ui basic button" href="{{ url('account/signin') }}">Log in</a>
                    <a class="ui basic button" href="{{ url('account/signup') }}">Sign Up</a>
                </div>
            </div>
        </div>

        <!-- Page Contents -->
        <div class="">
            {{ content() }}
        </div>

        {% include 'partials/footer.volt' %}
    </div>
{% endblock %}
