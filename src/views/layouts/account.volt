{% extends 'templates/base.volt' %}

{% block body %}
    <div class="ui dimmer">
        <div class="ui indeterminate text loader">Preparing account...</div>
    </div>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui icon header">
                <i class="lightning icon"></i>
                <a href="/">Semantic Web of Energy</a>
            </h2>
            {{ content() }}
        </div>
    </div>
{% endblock %}
