{{ form('admin/users/save/' ~ device._id, 'method': 'post', 'class': 'ui form', 'id': 'newUserForm') }}
<div class="ui grid">
    <div class="sixteen wide mobile eight wide computer column">
        <div class="field">
            <label for="name">Name</label>
            <div class="ui left corner labeled input">
                {{ text_field('name', 'placeholder': 'Name', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="surname">Surname</label>
            <div class="ui left corner labeled input">
                {{ text_field('surname', 'placeholder': 'Surname', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="company">Company</label>
            {{ text_field('company', 'placeholder': 'Company') }}
        </div>
        <div class="field">
            <label for="role">Role</label>
            <div class="ui left corner labeled input">
                {{ select('role', roles, 'class': 'ui fluid dropdown', 'using': ['id', 'name'], 'useEmpty': true, 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
    </div>
    <div class="sixteen wide mobile eight wide computer column">
        <div class="field">
            <label for="email">Email</label>
            <div class="ui left corner labeled input">
                {{ email_field('email', 'class': '', 'placeholder': 'Email', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="emailConfirmation">Email confirmation</label>
            <div class="ui left corner labeled input">
                {{ email_field('emailConfirmation', 'class': '', 'placeholder': 'Confirm email', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="password">Password </label>
            <div class="ui left corner labeled input">
                {{ password_field('password', 'class': '', 'placeholder': 'Password', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="passwordConfirmation">Password confirmation</label>
            <div class="ui left corner labeled input">
                {{ password_field('passwordConfirmation', 'class': '', 'placeholder': 'Confirm password', 'required': 'required') }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
    </div>
    <div class="sixteen wide column center aligned">
        {{ submit_button('Save', 'class': 'ui inverted green submit button save-user') }}
        <button type="reset" class="ui reset button cancel-user">Cancel</button>
    </div>
</div>
{{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
{{ end_form() }}
