{{ form('admin/users/update/' ~ user._id, 'method': 'post', 'class': 'ui form', 'id': 'editUserForm') }}
<div class="ui grid">
    <div class="sixteen wide mobile eight wide computer column">
        <div class="field">
            <label for="name">Name</label>
            <div class="ui left corner labeled input">
                {{ text_field('name', 'placeholder': 'Name', 'required': 'required', 'value': user.name) }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="surname">Surname</label>
            <div class="ui left corner labeled input">
                {{ text_field('surname', 'placeholder': 'Surname', 'required': 'required', 'value': user.surname) }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="company">Company</label>
            {{ text_field('company', 'placeholder': 'Company', 'value': user.company) }}
        </div>
        <div class="field">
            <label for="role">Role</label>
            <div class="ui left corner labeled input">
                {{ select('role', roles, 'class': 'ui fluid dropdown', 'using': ['id', 'name'], 'useEmpty': true, 'required': 'required', 'value': user.role) }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
    </div>
    <div class="sixteen wide mobile eight wide computer column">
        <div class="field">
            <label for="email">Email</label>
            <div class="ui left corner labeled input">
                {{ email_field('email', 'class': '', 'placeholder': 'Email', 'required': 'required', 'value': user.email) }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="emailConfirmation">Email confirmation</label>
            <div class="ui left corner labeled input">
                {{ email_field('emailConfirmation', 'class': '', 'placeholder': 'Confirm email', 'required': 'required', 'value': user.email) }}
                <div class="ui left corner label"><i class="asterisk icon"></i></div>
            </div>
        </div>
        <div class="field">
            <label for="password">Password </label>
            <div class="ui left corner labeled input">
                {{ password_field('password', 'class': '', 'placeholder': 'Password') }}
            </div>
        </div>
        <div class="field">
            <label for="passwordConfirmation">Password confirmation</label>
            <div class="ui left corner labeled input">
                {{ password_field('passwordConfirmation', 'class': '', 'placeholder': 'Confirm password') }}
            </div>
        </div>
    </div>
    <div class="sixteen wide column center aligned">
        {{ submit_button('Save', 'class': 'ui inverted green submit button save-user') }}
        {{ submit_button('Delete', 'class': 'ui inverted red button delete-user') }}
        <button type="reset" class="ui reset button cancel-user">Cancel</button>
    </div>
</div>
{{ hidden_field('user_id', 'value': user._id) }}
{{ hidden_field('delete', 'value': url('admin/users/delete/' ~ user._id)) }}
{{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
{{ end_form() }}

<div class="ui secondary pointing menu">
    <a class="{{ area.color }} active item" href="" title="Related devices">Related devices</a>
</div>


<table class="ui small sortable table device-list">
    <thead>
    <tr class="ui center aligned">
        <th>Id</th>
        <th>Name</th>
        <th>Location</th>
        <th>Charts</th>
        <th>Status</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
