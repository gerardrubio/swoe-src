<div class="ui simple dimmer">
    <div class="ui text loader">Loading</div>
</div>
<table class="ui small sortable table user-list">
    <thead>
    <tr class="ui center aligned">
        <th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Email</th>
        <th>Role</th>
        <th>Status</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
