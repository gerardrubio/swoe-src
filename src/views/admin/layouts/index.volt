{% extends '../templates/base.volt' %}

{% block body %}
    <!-- Sidebar Menu -->
    {% include "partials/sidebar-menu-admin.volt" %}

    {% include "partials/fixed-top-menu.volt" %}

    <div class="pusher">
        <div class="ui main container">
            {% include "partials/area-header.volt" %}
            {% include "partials/area-menu.volt" %}
            {{ content() }}
        </div>
    </div>

    {% include "../partials/footer.volt" %}

    <script>
        window.swoe = window.swoe || {};
        window.swoe.area = {
            color: '{{ area.color }}',
            icon: '{{ area.icon }}',
            id: '{{ area.id }}',
            label: '{{ area.label }}'
        };
    </script>
{% endblock %}
