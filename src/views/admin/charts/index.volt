<div class="ui grid">
    <div class="sixteen wide mobile three wide computer column">
        {{ form('class': 'ui form filter-chart') }}
        <div class="field">
            <label for="device">Device</label>
            <select class="ui search dropdown" name="device" id="device" placeholder="Device" required="required">
                {% if device %}
                    <option value="{{ device._id }}" selected="selected">{{ device.name }}</option>
                {% endif %}
            </select>
            <div class="results"></div>
        </div>
        <div class="field">
            <label for="dimension">Dimension</label>
            <select class="ui search dropdown" name="dimension" id="dimension" placeholder="Units of measure" required="required">
                {% if dimension %}
                    <option value="{{ dimension._id }}" selected="selected">{{ dimension.name }}</option>
                {% endif %}
            </select>
            <div class="results"></div>
        </div>
        <div class="field">
            <label for="from">From</label>
            <input type="date" class="datepicker" id="from" name="from" placeholder="Start date" data-value="{{ from }}">
        </div>
        <div class="field">
            <label for="until">Until</label>
            <input type="date" class="datepicker" id="until" name="until" placeholder="End date" data-value="{{ until }}">
        </div>
        <button class="ui icon button"><i class="filter icon"></i></button>
        {{ end_form() }}
    </div>
    <div class="sixteen wide mobile thirteen wide computer column chartcontainer">
        <div class="ui inverted dimmer">
            <div class="ui text loader">Loading</div>
        </div>
        <div class="readingschart" width="100%" height="350">
        </div>
    </div>
</div>
