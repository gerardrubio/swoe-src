{{ form('admin/devices/save', 'method': 'post', 'class': 'ui form', 'id': 'newDeviceForm') }}
<div class="ui grid">
    <div class="sixteen wide mobile eight wide computer column">
        <div class="field">
            <label for="name">Name</label>
            {{ text_field('name', 'placeholder': 'Name', 'required': 'required') }}
        </div>
        <div class="field">
            <label for="description">Description</label>
            {{ text_area('description', 'placeholder': 'Description') }}
        </div>
        <div class="field">
            <label for="owner">Owner</label>
            <select name="owner" id="owner" class="ui search dropdown" placeholder="Owner" required="required" {% if not auth['isAdmin'] %}readonly="readonly"{% endif %}>
                {% if not auth['isAdmin'] %}
                <option value="{{ owner.id }}" selected="selected">{{ owner.fullName() }}</option>
                {% endif %}
            </select>
            <div class="results"></div>
        </div>
        <div class="field">
            <label for="address">Address</label>
            <div class="ui action input">
                {{ text_field('address', 'placeholder': 'Address', 'required': 'required') }}
                <button class="ui icon button geolocate" title="Geolocate">
                    <i class="location arrow icon"></i>
                </button>
                <button class="ui icon button geocode" title="Geocode">
                    <i class="marker icon"></i>
                </button>
            </div>
        </div>
        <div class="field">
            <label for="coordinates">Coordinates</label>
            <div class="ui action input">
                {{ text_field('coordinates', 'placeholder': '0.0, 0.0', 'required': 'required', 'pattern': '(\-?\d+(\.\d+)?)\,\s?(\-?\d+(\.\d+)?)') }}
                <button class="ui icon button reversegeocode" title="Reverse geocode">
                    <i class="marker icon"></i>
                </button>
            </div>
        </div>
        <div class="field">
            <label for="protocol">Protocol</label>
            <select name="protocol" id="protocol" class="ui search dropdown" placeholder="Protocol" required="required">
                <option value="{{ protocol._id }}" selected="selected">{{ protocol.name }}</option>
            </select>
            <div class="results"></div>
        </div>
        <div class="field">
            <label for="ip">IP Address</label>
            {{ text_field('ip', 'class': 'form-control', 'placeholder': '0.0.0.0 or 0:0:0:0:0:0:0:0', 'required': 'required', 'pattern': '(^(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.){3}(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))$)|(^([a-f0-9]{1,4}\:){7}[a-f0-9]{1,4})$', 'title':'IPv4 0.0.0.0 or IPv6 0:0:0:0:0:0:0:0') }}
        </div>
    </div>
    <div class="sixteen wide mobile eight wide computer column map" id="map"></div>
    <div class="sixteen wide mobile eight wide computer column center aligned">
        {{ submit_button('Save', 'class': 'ui inverted green submit button save-device') }}
        <button type="reset" class="ui reset button cancel-device">Cancel</button>
    </div>
</div>
{{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
{{ end_form() }}
