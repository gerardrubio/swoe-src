<div class="ui simple dimmer">
    <div class="ui text loader">Loading</div>
</div>
<table class="ui small sortable table device-list">
    <thead>
    <tr class="ui center aligned">
        <th>Id</th>
        <th>Name</th>
        <th>Location</th>
        <th>Charts</th>
        <th>Owner</th>
        <th>Status</th>
        <th>Options</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
