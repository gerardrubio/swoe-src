<div class="ui stackable {% if auth['isAdmin'] %}four{% else %}two{% endif %} column grid statistics dashboard">
    {% if auth['isAdmin'] %}
    <div class="statistic column center aligned">
        <div class="value">
            <i class="user orange icon"></i> {{ users }}
        </div>
        <div class="label">Users</div>
        <a class="ui orange basic button" href="{{ url('admin/users') }}">More info</a>
    </div>
    {% endif %}
    <div class="statistic column center aligned">
        <div class="value">
            <i class="plug olive icon"></i> {{ devices }}
        </div>
        <div class="label">Devices</div>
        <a class="ui olive basic button" href="{{ url('admin/devices') }}">More info</a>
    </div>
    {% if auth['isAdmin'] %}
    <div class="statistic column center aligned">
        <div class="value">
            <i class="filter purple icon"></i> {{ dimensions }}
        </div>
        <div class="label">Dimensions</div>
        <!--a class="ui purple basic button" href="">More info</a-->
    </div>
    {% endif %}
    <div class="statistic column center aligned">
        <div class="value">
            <i class="heartbeat green icon"></i> {{ readings }}
        </div>
        <div class="label">Readings</div>
        <a class="ui green basic button" href="{{ url('admin/charts') }}">More info</a>
    </div>
</div>
