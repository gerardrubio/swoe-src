<div class="ui grid">
    <div class="sixteen wide mobile three wide computer column">
        {{ form('class': 'ui form filter-map') }}
        {% if auth['isAdmin'] %}
        <div class="field">
            <label for="owner">Owner</label>
            <select class="ui search dropdown" name="owner" id="owner">
                {% if owner %}
                    <option value="{{ owner._id }}" selected="selected">{{ owner.name }}</option>
                {% endif %}
            </select>
            <div class="results"></div>
        </div>
        {% else %}
            {{ hidden_field('owner', 'value':auth['id']) }}
        {% endif %}
        <div class="field">
            <label for="device">Device</label>
            <select class="ui search dropdown" name="device" id="device">
                {% if device %}
                    <option value="{{ device._id }}" selected="selected">{{ device.name }}</option>
                {% endif %}
            </select>
            <div class="results"></div>
        </div>
        <button class="ui icon button"><i class="filter icon"></i></button>
        {{ end_form() }}
    </div>
    <div class="sixteen wide mobile thirteen wide computer column">
        <div class="ui inverted dimmer">
            <div class="ui text loader">Loading</div>
        </div>
        <div id="main-map" class="main-map"></div>
    </div>
</div>
