<div class="ui grid">
    <div class="sixteen wide mobile three wide computer column">
        {{ form('class': 'ui form filter-map') }}
        <div class="field">
            <label for="address">Address</label>
            <div class="ui action input">
                {{ text_field('address', 'placeholder': 'Address') }}
                <button class="ui icon button geolocate" title="Geolocate">
                    <i class="location arrow icon"></i>
                </button>
                <button class="ui icon button geocode" title="Geocode">
                    <i class="marker icon"></i>
                </button>
            </div>
        </div>
        <div class="field">
            <label for="coordinates">Coordinates</label>
            <div class="ui action input">
                {{ text_field('coordinates', 'placeholder': '0.0, 0.0', 'pattern': '(\-?\d+(\.\d+)?)\,\s?(\-?\d+(\.\d+)?)') }}
                <button class="ui icon button reversegeocode" title="Reverse geocode">
                    <i class="marker icon"></i>
                </button>
            </div>
        </div>
        <div class="field">
            <label for="radius">Radius (m)</label>
            <div class="ui icon input">
                {{ text_field('radius', 'placeholder': 'Radius in meters', 'value': 100, 'pattern': '\d+(\.\d+)?') }}
                <i class="bullseye icon"></i>
            </div>
        </div>
        <button class="ui icon button"><i class="filter icon"></i></button>
        {{ end_form() }}
    </div>
    <div class="sixteen wide mobile thirteen wide computer column">
        <div class="ui inverted dimmer">
            <div class="ui text loader">Loading</div>
        </div>
        <div id="main-map" class="main-map"></div>
    </div>
</div>
