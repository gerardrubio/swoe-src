{% if sectionMenu is not empty %}
<div class="ui secondary pointing menu">
    {% for item in sectionMenu %}
        <a class="{{ area.color }} {{ item.active ? 'active ':'' }}{{ item.disabled ? 'disabled ' : '' }}item" href="{{ url(item.url) }}" title="{{ item.label }}">{{ item.label }}</a>
    {% endfor %}
</div>
{% endif %}
