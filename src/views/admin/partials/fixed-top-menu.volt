<div class="ui fixed {{ area.color ? 'inverted ' ~ area.color : '' }} menu">
    <div class="ui container">
        <a class="toc item">
            <i class="sidebar icon"></i>
        </a>
        <div class="header item">
            <i class="lightning icon"></i>&nbsp;Semantic Web of Energy
        </div>
    </div>
</div>
