<h2 class="ui header">
    <i class="{{ area.icon }} {{ area.color }} icon"></i>
    <div class="content">
        {{ area.label }}
    </div>
</h2>
