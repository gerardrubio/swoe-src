<div class="ui vertical thin sidebar menu">
    <a class="{{ area.id == 'dashboard' ? 'active ' : '' }}item" href="{{ url('admin') }}"><i class="dashboard icon"></i> Dashboard</a>
    {% if auth['isAdmin'] %}
    <a class="{{ area.id == 'users' ? 'active ' : '' }}item" href="{{ url('admin/users') }}"><i class="user icon orange"></i> Users</a>
    {% endif %}
    <a class="{{ area.id == 'devices' ? 'active ' : '' }}item" href="{{ url('admin/devices') }}"><i class="plug icon olive"></i> Devices</a>
    <a class="{{ area.id == 'charts' ? 'active ' : '' }}item" href="{{ url('admin/charts') }}"><i class="line chart icon green"></i> Charts</a>
    <a class="{{ area.id == 'maps' ? 'active ' : '' }}item" href="{{ url('admin/maps') }}"><i class="world icon teal"></i> Maps</a>

    <!--a class="{{ area.id == 'preferences' ? 'active ' : '' }}item" href="{{ url('admin/preferences') }}"><i class="settings icon blue"></i> Preferences</a-->
    <a class="item" href="{{ url('account/logout') }}"><i class="sign out icon red"></i> Log out</a>
</div>
