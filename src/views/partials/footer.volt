<div class="ui inverted vertical footer segment">
<div class="ui container">
    <div class="ui stackable inverted divided equal height stackable center aligned grid">
        <div class="three wide column">
            <h4 class="ui inverted header">About</h4>
            <div class="ui inverted link list">
                <a class="item" href="#">Sitemap</a>
                <a class="item" href="#">Terms and Conditions</a>
                <a class="item" href="#">Privacy Policy</a>
                <a class="item" href="#">Contact Us</a>
            </div>
        </div>
        <div class="three wide column">
            <h4 class="ui inverted header">Connect</h4>
            <div class="ui inverted link list">
                <a href="#" class="item" title="Facebook"><i class="facebook icon"></i> Facebook</a>
                <a href="#" class="item" title="LinkedIn"><i class="linkedin icon"></i> LinkedIn</a>
                <a href="#" class="item" title="Twitter"><i class="twitter icon"></i> Twitter</a>
            </div>
        </div>
        <div class="seven wide column center aligned">
            <h4 class="ui inverted header"><i class="lightning normal icon ui centered" title="Semantic Web of Energy"></i> Semantic Web of Energy</h4>
            <p><strong>Copyright &copy; 2015 <a href="/">Semantic Web of Energy</a>.</strong> All rights reserved.</p>
        </div>
    </div>
</div>
