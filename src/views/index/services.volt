<div class="ui vertical masthead center aligned segment">
</div>

<div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
        <div class="row">
            <div class="ten wide column">
                <h3 class="ui header">Internet of Things</h3>
                <p>The Internet of Things (IoT), also called Internet of Everything or Network of Everything, is the network of physical objects or "things" embedded with electronics, software, sensors, and connectivity to enable objects to exchange data with the production, operator and/or other connected devices based on the infrastructure of International Telecommunication Union's Global Standards Initiative. The Internet of Things allows objects to be sensed and controlled remotely across existing network infrastructure, creating opportunities for more direct integration between the physical world and computer-based systems, and resulting in improved efficiency, accuracy and economic benefit. Each thing is uniquely identifiable through its embedded computing system but is able to interoperate within the existing Internet infrastructure. Experts estimate that the IoT will consist of almost 50 billion objects by 2020.</p>
            </div>
            <div class="six wide column">
                <img src="/img/iot.png" alt="Internet of Things"/>
            </div>
        </div>
    </div>
</div>

<div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
        <div class="row">
            <div class="six wide column">
                <img src="/img/ioe.jpg" alt="Internet of Things - Energy"/>
            </div>
            <div class="right floated left aligned ten wide column">
                <h3 class="ui header">IoT - Energy</h3>
                <p>Integration of sensing and actuation systems, connected to the Internet, is likely to optimize energy consumption as a whole. It is expected that IoT devices will be integrated into all forms of energy consuming devices (switches, power outlets, bulbs, televisions, etc.) and be able to communicate with the utility supply company in order to effectively balance power generation and energy usage. Such devices would also offer the opportunity for users to remotely control their devices, or centrally manage them via a cloud based interface, and enable advanced functions like scheduling (e.g., remotely powering on or off heating systems, controlling ovens, changing lighting conditions etc.). In fact, a few systems that allow remote control of electric outlets are already available in the market, e.g., Belkin's WeMo, Ambery Remote Power Switch, Budderfly, Telkonet's EcoGuard etc.</p>
                <p>Besides home based energy management, the IoT is especially relevant to the Smart Grid since it provides systems to gather and act on energy and power-related information in an automated fashion with the goal to improve the efficiency, reliability, economics, and sustainability of the production and distribution of electricity. Using Advanced Metering Infrastructure (AMI) devices connected to the Internet backbone, electric utilities can not only collect data from end-user connections, but also manage other distribution automation devices like transformers and reclosers.</p>
            </div>
        </div>
    </div>
</div>
