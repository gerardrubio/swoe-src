<div class="ui vertical stripe segment forbidden">
    <div class="ui middle aligned stackable grid container">
        <div class="centered column row">
            <i class="massive red minus circle icon"></i>
        </div>
        <div class="centered column row">
        <h3 class="ui header">The page you were trying to access is for members only.</h3>
        <p>Meanwhile, you may <a href="{{ url('account/signin') }}">log in</a>, <a href="{{ url('account/signup') }}">sign up</a> or <a href="{{ url() }}">return to the home page</a>.</p>
        </div>
    </div>
</div>
