<div class="ui vertical stripe missing">
    <div class="ui middle aligned stackable grid container">
        <div class="centered column row">
            <i class="massive grey comments icon"></i>
        </div>
        <div class="centered column row">
            <h3 class="ui header">Need help?</h3>
            <p>Please fill out the form below with any enquiries you may have. We'll make sure to reply as soon as possible!</p>
        </div>
    </div>
</div>

<div class="ui middle aligned center grid">
    <div class="column">
        {{ form('contact', 'action':'contact-submit', 'method' : 'post', 'class': 'ui form') }}
        <div class="ui stacked segment container">
            <div class="field">
                <div class="ui left icon input">
                    <i class="user icon"></i>
                    {{ text_field('name', 'placeholder': 'Your name', 'required': 'required') }}
                </div>
            </div>
            <div class="field">
                <div class="ui left icon input">
                    <i class="mail icon"></i>
                    {{ email_field('email', 'placeholder': 'E-mail address', 'required': 'required') }}
                </div>
            </div>
            <div class="field">
                <div class="ui input">
                    {{ text_area('content', 'placeholder': 'Your message / question', 'required': 'required') }}
                </div>
            </div>
            <div class="field">
                <div class="ui toggle checkbox">
                    {{ check_field('terms', 'class': 'hidden', 'value':1) }}
                    <label>I agree to the <a href="">terms and conditions</a></label>
                </div>
            </div>
            <div class="g-recaptcha" data-sitekey="6LcLDwwTAAAAAOpsx-Zec1jCpraMry94mfKBiygu"></div>
            {{ submit_button('Send', 'class': 'ui positive submit button') }}
        </div>

        <div class="ui error message"></div>

        {{ hidden_field(security.getTokenKey(), "value": security.getToken()) }}
        {{ endform() }}
    </div>
</div>

<div class="ui dimmer">
    <div class="ui indeterminate text loader">Sending your message</div>
</div>
