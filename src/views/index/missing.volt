<div class="ui vertical stripe segment missing">
    <div class="ui middle aligned stackable grid container">
        <div class="centered column row">
            <i class="massive grey spy icon"></i>
        </div>
        <div class="centered column row">
            <h3 class="ui header">We could not find the page you were looking for.</h3>
            <p>Meanwhile, you may <a href="#" onclick="window.history.go(-1); return false;">go back</a> or <a href="{{ url() }}">return to the home page</a>.</p>
        </div>
    </div>
</div>
