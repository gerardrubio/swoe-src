<div class="ui vertical masthead center aligned segment">

    <div class="ui text container header-title">
        <h1 class="ui header">Semantic Web of Energy</h1>
        <h2>(Almost) Real Time Energy Information</h2>
    </div>

    <div id="header-map" class="header-map"></div>
</div>

<div class="ui vertical stripe segment">
    <div class="ui middle aligned stackable grid container">
        <div class="row">
            <div class="eight wide column">
                <h3 class="ui header">Visualise your energy consumption</h3>
                <p>With SWoE you can visualise with ease your energy consumption, wherever, whenever.</p>
            </div>
        </div>
    </div>
</div>
