<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 23/08/2015
 * Time: 22:54
 */

namespace Swoe\Exceptions;

class UserNotFoundException extends \Exception {}
