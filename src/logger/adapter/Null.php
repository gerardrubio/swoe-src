<?php

namespace Swoe\Logger\Adapter;

/**
 * Phalcon\Logger\Adapter\Null
 * Sends logs nowhere
 * <code>
 * $logger = new \Phalcon\Logger\Adapter\Null('a log name');
 * $logger->log("This is a message");
 * $logger->log("This is an error", \Phalcon\Logger::ERROR);
 * $logger->error("This is another error");
 * </code>
 */
class Null extends \Phalcon\Logger\Adapter implements \Phalcon\Logger\AdapterInterface
{
    /**
     * Phalcon\Logger\Adapter\Stream constructor
     *
     * @param string $name
     * @param array $options
     */
    public function __construct($name, $options = null) {}

    /**
     * Returns the internal formatter
     *
     * @return \Phalcon\Logger\FormatterInterface
     */
    public function getFormatter() {}

    /**
     * Writes the log to the stream itself
     *
     * @param string $message
     * @param int $type
     * @param int $time
     * @param array $context
     */
    public function logInternal($message, $type, $time, $context) {}

    /**
     * Closes the logger
     *
     * @return bool
     */
    public function close() {}

}
