<?php

namespace Swoe\Plugins;

use DateTime;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Swoe\Exceptions\EmptyAnnotationParameterException;

/**
 * Enables the cache for a view if the latest
 * executed action has the annotation @Cache
 */
class CacheEnablerPlugin extends Plugin
{
    const ANNOTATION_NAME           = 'Cache';
    const DEFAULT_CACHE_KIND        = 'data';
    const HEADER_IF_NONE_MATCH      = 'If-None-Match';
    const HEADER_EXPIRES            = 'Expires';
    const PARAMETER_CACHE_KEY       = 'key';
    const PARAMETER_CACHE_KIND      = 'kind';
    const PARAMETER_CACHE_LIFETIME  = 'lifetime';
    const REQUEST_IS_CACHED         = 'cached';
    const SERVICE_PREFIX            = 'cache.';

    const KEY_GLUE = '|';
    private $config = [];

    public function __construct($config) {
        $this->config = $config;
    }

    /**
     * Obtain the key for the current action
     * @param string $controller
     * @param string $method
     * @param array $params
     * @return string
     */
    private function generateKey($controller = '', $method = '', $params = []) {
        $auth = $this->session->get('auth');
        if ($auth) {
            if (is_string($auth['id']))
                $authId = '';
            else {
                $idField = '$id';
                $authId = $auth['id']->$idField;
            }
        } else {
            $authId = '';
        }

        $key = $authId . static::KEY_GLUE . $controller . static::KEY_GLUE . $method . static::KEY_GLUE . implode(static::KEY_GLUE, $params);
        $this->logger->log('CACHE: Key before md5: ' . $key);
        return md5($key);
    }

    /**
     * Obtain the cache repository
     * @return mixed
     */
    private function getCache() {
        return $this->getDI()->get(static::SERVICE_PREFIX . $this->cache[static::PARAMETER_CACHE_KIND]);
    }

    /**
     * This event is executed before every action is executed in the dispatcher
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        // Parse the annotations in the method currently executed
        $annotations = $this->annotations->getMethod(
            $dispatcher->getControllerClass(),
            $dispatcher->getActiveMethod()
        );

        // Check if the method has an annotation 'Cache'
        if ($annotations->has(static::ANNOTATION_NAME)) {
            $this->logger->log('CACHE: Annotation cache found');
            // The method has the annotation 'Cache'
            $annotation = $annotations->get(static::ANNOTATION_NAME);
            $this->cache = [];

            // Check if there is a user defined cache key
            try {
                $this->cache[static::PARAMETER_CACHE_KEY] = $annotation->getNamedParameter(static::PARAMETER_CACHE_KEY);
                if (empty($this->cache[static::PARAMETER_CACHE_KEY]))
                    throw new EmptyAnnotationParameterException;
                $this->logger->log('CACHE: Override key ' . $this->cache[static::PARAMETER_CACHE_KEY]);
            } catch (EmptyAnnotationParameterException $e) {
                $controller = $dispatcher->getControllerClass();
                $method = $dispatcher->getActiveMethod();
                $params = $dispatcher->getParams();
                $this->cache[static::PARAMETER_CACHE_KEY] = $this->generateKey($controller, $method, $params);
                $this->logger->log('CACHE: Default key ' . $this->cache[static::PARAMETER_CACHE_KEY]);
            }

            try {
                $this->cache[static::PARAMETER_CACHE_KIND] = $annotation->getNamedParameter(static::PARAMETER_CACHE_KIND);
                if (empty($this->cache[static::PARAMETER_CACHE_KIND]))
                    throw new EmptyAnnotationParameterException;
                $this->logger->log('CACHE: Kind ' . $this->cache[static::PARAMETER_CACHE_KIND]);
            } catch (EmptyAnnotationParameterException $e) {
                $this->cache[static::PARAMETER_CACHE_KIND] = static::DEFAULT_CACHE_KIND;
                $this->logger->log('CACHE: default kind ' . $this->cache[static::PARAMETER_CACHE_KIND]);
            }

            // Get the lifetime
            try {
                $this->cache[static::PARAMETER_CACHE_LIFETIME] = $annotation->getNamedParameter(static::PARAMETER_CACHE_LIFETIME);
                if (empty($this->cache[static::PARAMETER_CACHE_LIFETIME]))
                    throw new EmptyAnnotationParameterException;
                $this->logger->log('CACHE: Override lifetime ' . $this->cache[static::PARAMETER_CACHE_LIFETIME]);
            } catch (EmptyAnnotationParameterException $e) {
                $this->cache[static::PARAMETER_CACHE_LIFETIME] = $this->config[$this->cache[static::PARAMETER_CACHE_KIND]][static::PARAMETER_CACHE_LIFETIME];
                $this->logger->log('CACHE: default lifetime ' . $this->cache[static::PARAMETER_CACHE_LIFETIME]);
            }

            $repository = $this->getCache();

            if ($repository->exists($this->cache[static::PARAMETER_CACHE_KEY])) {
                $this->logger->log('CACHE: Key found!');
                $this->cache[static::REQUEST_IS_CACHED] = true;

                if ($this->isCacheFresh($this->request->getHeaders())) {
                    $this->logger->log('CACHE: Browser still has a valid copy! 304!');
                    $this->response->setStatusCode(304);
                    return;
                }

                $this->logger->log('CACHE: Browser has a stale copy :( Sending a server-side cached version.');
                $this->response->setContent($repository->get($this->cache[static::PARAMETER_CACHE_KEY]));
                return;
            }

            $this->cache[static::REQUEST_IS_CACHED] = false;
            $this->logger->log('CACHE: Key not found... proceeding with action');
        }
    }

    private function isCacheFresh(array $headers, $maxTimeout = -1) {
        if ($maxTimeout < 0) {
            $maxTimeout = time();
        }

        $timeout = 0;
        if (array_key_exists(static::HEADER_IF_NONE_MATCH, $headers)) {
            $timeout = intval($headers[static::HEADER_IF_NONE_MATCH]);
        }

        if (array_key_exists(static::HEADER_EXPIRES, $headers)) {
            $max_date = strtotime($headers[static::HEADER_EXPIRES]);
            $due = new DateTime();
            $due = $due->setTimestamp($max_date);
            $timeout = $due->getTimestamp();
        }

        return $timeout - $maxTimeout > 0;
    }

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function afterExecuteRoute(Event $event, Dispatcher $dispatcher) {
        if (!isset($this->cache)) // estamos en un redirect
            return;

        if ($this->cache[static::REQUEST_IS_CACHED])
            return;

        $statusCode = $this->response->getStatusCode();
        if ($statusCode && stripos($statusCode, '403') >= 0)  // es un 403 (forbidden), no lo queremos cachear
            return;

        if ($statusCode && stripos($statusCode, '404') >= 0)  // es un 404, no lo queremos cachear
            return;

        try {
            $repository = $this->getCache();
            $repository->save($this->cache[static::PARAMETER_CACHE_KEY], $this->response->getContent(), $this->cache[static::PARAMETER_CACHE_LIFETIME]);
            $this->logger->log('CACHE: Saved key ' . $this->cache[static::PARAMETER_CACHE_KEY]);

            $etag = time() + $this->cache[static::PARAMETER_CACHE_LIFETIME];
            $this->response->setEtag($etag);
            $future = new \DateTime();
            $future = $future->modify('+' . $this->cache[static::PARAMETER_CACHE_LIFETIME] . ' seconds');
            $this->response->setExpires($future);
            $this->logger->log('CACHE: Expires set for ' . $future->getTimestamp() . ' (now is ' . time() . ')');
        } catch (\Exception $e) {
            $this->logger->error('CACHE: Exception thrown while saving to cache!');
            $this->logger->error($e->getMessage());
            $this->logger->error($e->getTraceAsString());
        }
    }
}
