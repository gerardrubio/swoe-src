<?php

namespace Swoe\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;
use Swoe\Exceptions\EmptyAnnotationParameterException;
use Swoe\Models\Users\RolesInterface;

class RoleSecurityPlugin extends Plugin
{
    const AUTH_KEY = 'auth';
    const AUTH_ROLE = 'role';

    const ANNOTATION_NAME = 'Security';
    const PARAMETER_ROLE_KEY = 'role';
    const PARAMETER_ROLE_PIPE = '|';

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get(static::AUTH_KEY);
        if (!$auth) {
            $auth = [
                'id' => '',
                'email' => '',
                'name' => '',
                'role' => RolesInterface::VISITOR,
                'remember' => false
            ];
            $this->session->set(static::AUTH_KEY, $auth);
        }

        // Parse the annotations in the method currently executed
        $annotations = $this->annotations->get($dispatcher->getControllerClass());
        $classAnnotations = $annotations->getClassAnnotations();
        $this->logger->log('SECURITY: Checking if controller '  . $dispatcher->getControllerClass() . ' has annotations');
        if ($classAnnotations->has(static::ANNOTATION_NAME)) {
            $this->logger->log('SECURITY: We have annotations :)');
            $annotation = $classAnnotations->get(static::ANNOTATION_NAME);
            $this->logger->log('SECURITY: Checking which action is running: '  . $dispatcher->getActionName());
            try {
                $rolesAnnotated = $annotation->getNamedParameter(static::PARAMETER_ROLE_KEY);
                if (empty($rolesAnnotated))
                    throw new EmptyAnnotationParameterException;

                $this->logger->log('SECURITY: Role must match ' . $rolesAnnotated);
                $rolesAnnotated = explode(static::PARAMETER_ROLE_PIPE, $rolesAnnotated);
                $rolesAllowed = 0;
                foreach($rolesAnnotated as $role) {
                    $this->logger->log('SECURITY: > Role ' . $role . ' - ' . decbin($role));
                    $rolesAllowed |= $role;
                }

                $this->logger->log('SECURITY: >> Role must match ' . $rolesAllowed . ' - ' . decbin($rolesAllowed));
            } catch (EmptyAnnotationParameterException $e) {
                $rolesAllowed = RolesInterface::VISITOR;
            }

            $this->logger->log('SECURITY: Auth\'d user has role ' . $auth[static::AUTH_ROLE] . ' - ' . decbin($auth[static::AUTH_ROLE]));
            $match = $auth[static::AUTH_ROLE] &= $rolesAllowed;

            $this->logger->log('SECURITY: Is role allowed? ' . $match . ' - ' . decbin($match));

            if ($auth[static::AUTH_ROLE] &= $rolesAllowed) {
                $this->logger->log('SECURITY: user is ALLOWED!');
                return;
            }

            $this->logger->log('SECURITY: user is FORBIDDEN!');
            $this->response->setStatusCode(403);
            $this->dispatcher->forward([
                'namespace' => 'Swoe\\Controllers',
                'controller' => 'Index',
                'action'    => 'forbidden'
            ]);

            return;
        }

        $this->logger->log('SECURITY: No annotations at all. Maybe it\'s public?');
    }
}
