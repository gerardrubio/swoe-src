<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces(array(
	'Swoe\Controllers' => __DIR__ . '/../controllers/',
    'Swoe\Controllers\Admin' => __DIR__ . '/../controllers/admin/',
    'Swoe\Models' => __DIR__ . '/../models/'
))->register();
