<?php

use \Phalcon\Mvc\Router;
use \Phalcon\Mvc\Router\Annotations;

$router = new Annotations(false);

$router->removeExtraSlashes(true);

$router->addResource('Swoe\\Controllers\\Account', '/account');
$router->addResource('Swoe\\Controllers\\Admin\\Charts', '/admin/charts');
$router->addResource('Swoe\\Controllers\\Admin\\Devices', '/admin/devices');
$router->addResource('Swoe\\Controllers\\Admin\\Dimensions', '/admin/dimensions');
$router->addResource('Swoe\\Controllers\\Admin\\Maps', '/admin/maps');
$router->addResource('Swoe\\Controllers\\Admin\\Protocols', '/admin/protocols');
$router->addResource('Swoe\\Controllers\\Admin\\Users', '/admin/users');
$router->addResource('Swoe\\Controllers\\Admin\\Index', '/admin');
$router->addResource('Swoe\\Controllers\\Index');

$router->notFound([
    'namespace'     => 'Swoe\\Controllers',
    'controller'    => 'Index',
    'action'        => 'missing',
]);

return $router;
