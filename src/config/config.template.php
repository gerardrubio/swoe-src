<?php

return new \Phalcon\Config([
	'database' => [
        'connectionString'  => 'mongodb://mongo.swoe.eu:27017',
		'dbname'      => 'swoe',
	],
	'application' => [
		'controllersDir' => __DIR__ . '/../../app/controllers/',
		'modelsDir'      => __DIR__ . '/../../app/models/',
		'viewsDir'       => __DIR__ . '/../../app/views/',
		'pluginsDir'     => __DIR__ . '/../../app/plugins/',
		'libraryDir'     => __DIR__ . '/../../app/library/',
		'cacheDir'       => __DIR__ . '/../../app/cache/',
		'baseUri'        => '/',
	],
    'volt' => [
        'cache' => __DIR__ . '/../../app/cache/',
        'compileAlways' => true,
    ],
    'email' => [
        'username'      => 'no-reply@swoe.eu',
        'password'      => '',
        'host'          => '',
        'port'          => 25,
        'encryption'    => '',
    ],
    'cache' => [
        'data' => [
            'cacheDir'      => __DIR__ . '/../../app/cache/data/',
            'lifetime'      => 60,
            'host'          => 'cache.swoe.eu',
            'port'          => 11211,
            'persistent'    => false,
        ],
        'json' => [
            'cacheDir'      => __DIR__ . '/../../app/cache/json/',
            'lifetime'      => 30,
            'host'          => 'cache.swoe.eu',
            'port'          => 11211,
            'persistent'    => false,
        ]
    ],
    'logger' => [
        'enabled'   => false,
        'log'       => '/var/log/nginx/error.log'
    ],
    'session' => [
        'duration'  => 30
    ]
]);
