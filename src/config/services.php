<?php

use Phalcon\Cache\Backend\Libmemcached,
    Phalcon\Cache\Frontend\Data as DataCache,
    Phalcon\Cache\Frontend\Json as JsonCache,
    Phalcon\DI\FactoryDefault,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Flash\Session as FlashSession,
    Phalcon\Logger\Adapter\Stream as StreamLogger,
    Phalcon\Mvc\Collection\Manager,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\Url as UrlResolver,
    Phalcon\Mvc\View,
    Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Session\Adapter\Files as FileSession,
    Swoe\Logger\Adapter\Null as NullLogger,
    Swoe\Plugins\CacheEnablerPlugin,
    Swoe\Plugins\RoleSecurityPlugin;
use Swoe\Plugins\MultipleHandlerPlugin;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('router', function(){
	return require __DIR__ . '/routes.php';
}, true);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config) {
	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);
	return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function() use ($config) {

	$view = new View();

	$view->setViewsDir($config->application->viewsDir);

	$view->registerEngines(array(
		'.volt' => function($view, $di) use ($config) {

			$volt = new VoltEngine($view, $di);

			$volt->setOptions([
                'compileAlways' => $config->volt->compileAlways,
				'compiledPath' => $config->volt->cache,
				'compiledSeparator' => '_',
            ]);

			return $volt;
		}
	));

	return $view;
}, true);

$di->set('mongo', function() use ($config) {
    $mongo = new \MongoClient($config->database->connectionString);
    return $mongo->selectDB($config->database->dbname);
}, true);

$di->set('collectionManager', function(){
    return new Manager();
}, true);

$di->set('session', function() use ($config) {
    $session = new FileSession();
    session_cache_limiter('private, must-revalidate');
    session_cache_expire($config->session->duration); // in minutes
    $session->start();

    return $session;
});

$di->set('dispatcher', function(){
	$dispatcher = new Dispatcher();
	$dispatcher->setDefaultNamespace('Swoe\Controllers');

	return $dispatcher;
});

$di->set('flash', function () {
    $flash = new FlashSession([
        'error'   => 'callout callout-danger',
        'success' => 'callout callout-success',
        'notice'  => 'callout callout-info',
        'warning' => 'callout callout-warning'
    ]);

    return $flash;
});

$di->set('email', function() use ($config) {
    //http://stackoverflow.com/questions/18129664/swiftmailer-config-send-mail-using-gmail
    $transport = Swift_SmtpTransport::newInstance($config->email->host,
            $config->email->port,
            $config->email->encryption)
        ->setUsername($config->email->username)
        ->setPassword($config->email->password);

    $email = Swift_Mailer::newInstance($transport);
    return $email;
});

$di->set('cache.data', function() use ($config) {
    $frontend = new DataCache([
        $config->cache->data->lifetime
    ]);

    //Create the Cache setting memcached connection options
    $cache = new Libmemcached($frontend, [
        'host'          => $config->cache->data->host,
        'port'          => $config->cache->data->port,
        'persistent'    => $config->cache->data->persistent,
        'statsKey'      => ''
    ]);
/*
    $cache = new \Phalcon\Cache\Backend\File($frontend, [
        'cacheDir' => $config->cache->data->cacheDir
    ]);
*/
    return $cache;
});

$di->set('cache.json', function() use ($config) {
    $frontend = new JsonCache([
        $config->cache->json->lifetime
    ]);

    //Create the Cache setting memcached connection options
    $cache = new Libmemcached($frontend, [
        'host'          => $config->cache->json->host,
        'port'          => $config->cache->json->port,
        'persistent'    => $config->cache->json->persistent,
        'statsKey'      => ''
    ]);
    /*
    $cache = new \Phalcon\Cache\Backend\File($frontend, [
        'cacheDir' => $config->cache->json->cacheDir
    ]);
    */
    return $cache;
});

$di->set('dispatcher', function () use ($config) {

    $eventsManager = new EventsManager();

    $cachePlugin = new CacheEnablerPlugin($config->cache);

    $eventsManager->attach('dispatch:beforeDispatch', new RoleSecurityPlugin);
    $eventsManager->attach('dispatch:beforeExecuteRoute', $cachePlugin);
    $eventsManager->attach('dispatch:afterExecuteRoute', $cachePlugin);

    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});

$di->set('logger', function() use ($config) {
    if ($config->logger->enabled) {
        $logger = new StreamLogger($config->logger->log);
        $logger->setFormatter(new \Phalcon\Logger\Formatter\Line('[%date%][%type%] %message%' . PHP_EOL));
    } else
        $logger = new NullLogger($config->logger->log);

    return $logger;
});
