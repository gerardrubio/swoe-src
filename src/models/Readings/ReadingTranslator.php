<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 18/08/2015
 * Time: 0:46
 */

namespace Swoe\Models\Readings;


use Swoe\Models\Dimensions\DimensionInterface;
use Swoe\Models\Protocols\ProtocolInterface;

class ReadingTranslator {

    /**
     * @param array $readings
     * @param DimensionInterface $dimension
     * @return array
     */
    public static function translate(array $readings, DimensionInterface $dimension, ProtocolInterface $protocol) {
        $translation = [];

        $protocolClass = 'Swoe\\Models\\Protocols\\' . $protocol->name . 'Protocol';
        $protocolReader = new $protocolClass();

        foreach($readings as $reading) {
            $translation[] = new Translation($protocolReader->readValue($reading), $dimension->units, $reading->timestamp);
        }

        usort($translation, function($a, $b) {
            return $a->timestamp() < $b->timestamp();
        });

        return $translation;
    }
}
