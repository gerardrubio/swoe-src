<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 18/08/2015
 * Time: 0:48
 */

namespace Swoe\Models\Readings;


interface TranslationInterface {
    public function units();
    public function timestamp();
    public function value();
    public function __toString();
}
