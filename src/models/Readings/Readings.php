<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Readings;

use Swoe\Models\Document;
use Swoe\Models\StatusInterface;

class Readings extends Document implements ReadingInterface {

    public $protocol;
    public $device;
    public $data;
    public $timestamp;

    /**
     * @param mixed $value
     * @return bool
     */
    static function exists($value)
    {
        // TODO: Implement exists() method.
    }

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw)
    {
        // TODO: Implement sanitized() method.
    }

    function sanitize()
    {
        // TODO: Implement sanitize() method.
    }
}
