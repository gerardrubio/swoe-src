<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 18/08/2015
 * Time: 0:49
 */

namespace Swoe\Models\Readings;


use JsonSerializable;

class Translation implements TranslationInterface, JsonSerializable {

    protected $timestamp;
    protected $units;
    protected $value;

    public function __construct($value, $units, $timestamp) {
        $this->timestamp = $timestamp;
        $this->units = $units;
        $this->value = $value;
    }

    public function units()
    {
        return $this->units;
    }

    public function value()
    {
        return $this->value;
    }

    public function __toString()
    {
        return sprintf("%f %s", $this->value, $this->units);
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return [
            'formatted' => $this->__toString(),
            'timestamp' => $this->timestamp(),
            'units'     => $this->units(),
            'value'     => $this->value()
        ];
    }

    public function timestamp()
    {
        return $this->timestamp;
    }
}
