<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 11:19
 */

namespace Swoe\Models\Readings;

use Swoe\Models\DocumentInterface;

interface ReadingInterface extends DocumentInterface {

}
