<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 01/08/2015
 * Time: 11:48
 */

namespace Swoe\Models;

use MongoDate;
use Phalcon\Mvc\Collection;
use Swoe\Exceptions\DocumentNotFoundException;

abstract class Document
    extends
        Collection
    implements
        DocumentInterface,
        PagingInterface,
        ExistenceInterface,
        FindableInterface,
        SanitizableInterface,
        ActiveInterface {

    public $updated = null;
    public $status = StatusInterface::NOT_CONFIRMED;

    // PagingInterface
    /**
     * @param int $page
     * @param int $size
     * @param int $status
     * @return array
     */
    public static function page($page = PagingInterface::DEFAULT_PAGE, $size = PagingInterface::DEFAULT_PAGE_SIZE, $status = StatusInterface::FILTER_ANY) {
        $search_params = [
            'limit' => $size,
            'skip'  => $page * $size,
            'conditions' => [
                '$where' => 'this.status & ' . $status
            ]
        ];

        $documents = static::find($search_params);

        return $documents;
    }

    public static function pages($size = PagingInterface::DEFAULT_PAGE_SIZE)
    {
        return ceil(static::total() / $size);
    }

    public static function total() {
        return static::count();
    }
    // PagingInterface

    // FindableInterface
    public static function findBy($fields = [], $limit = FindableInterface::ALL_DOCUMENTS, $skip = FindableInterface::SKIP_NONE, $returnFields = []) {
        $search_params = [
            'conditions' => $fields,
            'limit' => $limit,
            'skip' => $skip,
            'fields' => $returnFields
        ];

        $documents = static::find($search_params);

        return $documents;
    }

    public static function findOne($field, $value) {
        $documents = static::findBy([$field => $value], FindableInterface::ONE_DOCUMENT);

        if (count($documents) > 0)
            return $documents[0];

        throw new DocumentNotFoundException;
    }

    /**
     * Find a random element of the collection
     * @return mixed
     * @throws DocumentNotFoundException
     */
    public static function findRandom() {
        $documentIndex = floor(mt_rand(0, static::count()));
        $documents = static::find([
            'limit' => -1,
            'skip' => $documentIndex
        ]);

        if (count($documents) > 0)
            return $documents[0];

        throw new DocumentNotFoundException;
    }
    // FindableInterface

    // ActiveInterface
    public function isActive() {
        return $this->status & StatusInterface::FILTER_CONFIRMED;
    }

    public function save() {
        $this->updated = new MongoDate(time());
        return parent::save();
    }
    // ActiveInterface

    public function delete() {
        $this->status = StatusInterface::DELETED;
        $this->save();

        return true;
    }

    public function idTimestamp() {
        return $this->getId()->getTimestamp();
    }

    public static function deleteById($id) {
        $document = self::findById($id);
        if ($document) {
            return $document->delete();
        }

        return false;
    }

    public static function queryGenerator(array $fragments) {
        return implode(' && ', $fragments);
    }
}
