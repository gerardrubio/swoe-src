<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 22/08/2015
 * Time: 17:33
 */

namespace Swoe\Models\Views\Admin;


class Area {
    public $color = '';
    public $icon = '';
    public $id = '';
    public $label = '';
}
