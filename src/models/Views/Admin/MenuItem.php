<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 22/08/2015
 * Time: 16:52
 */

namespace Swoe\Models\Views\Admin;


class MenuItem {
    public $active = false;
    public $disabled = false;
    public $url = '';
    public $label = '';
}
