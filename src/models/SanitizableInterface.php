<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 10:52
 */

namespace Swoe\Models;

interface SanitizableInterface {

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw);

    function sanitize();
}
