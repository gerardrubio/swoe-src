<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Dimensions;

use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Models\Document;
use Swoe\Models\StatusInterface;

class Dimensions extends Document implements DimensionInterface {

    public $name;
    public $units;
    public $protocol;
    public $matcher;
    public $default = false;

    /**
     * @param mixed $value
     * @return bool
     */
    static function exists($value)
    {
        // TODO: Implement exists() method.
    }

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw)
    {
        // TODO: Implement sanitized() method.
    }

    function sanitize()
    {
        // TODO: Implement sanitize() method.
    }

    public function aggregationQuery() {
        $matcherFragments = explode(' && ', $this->matcher);
        $fragmentCount = count($matcherFragments);
        $fragments = [];
        for($i = 0; $i < $fragmentCount; $i++) {
            $field = explode(' == ', $matcherFragments[$i]);
            $fragments[str_replace('this.', '', $field[0])] = intval($field[1], 10);
        }

        return $fragments;
    }

    public static function getDefault() {
        $dimension = Dimensions::findFirst([
            'conditions' => [
                'default' => true
            ]
        ]);

        if (empty($dimension))
            throw new DocumentNotFoundException;

        return $dimension;
    }
}
