<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Protocols;

use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Models\Document;
use Swoe\Models\Readings\ReadingInterface;

class Protocols extends Document implements ProtocolInterface {

    public $name;
    public $default = false;

    public static function getDefault() {
        $protocol = Protocols::findFirst([
            'conditions' => [
                'default' => true
            ]
        ]);

        if (empty($protocol))
            throw new DocumentNotFoundException;

        return $protocol;
    }

    /**
     * @param mixed $value
     * @return bool
     */
    static function exists($value)
    {
        // TODO: Implement exists() method.
    }

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw)
    {
        // TODO: Implement sanitized() method.
    }

    function sanitize()
    {
        // TODO: Implement sanitize() method.
    }

    public function readValue(ReadingInterface $reading)
    {
        // TODO: Implement readValue() method.
    }
}
