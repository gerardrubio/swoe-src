<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 18/08/2015
 * Time: 1:03
 */

namespace Swoe\Models\Protocols;


use Swoe\Models\Readings\ReadingInterface;

class OBISProtocol implements ProtocolInterface {

    const VALUE_FIELD = 'f';

    public function readValue(ReadingInterface $reading)
    {
        return $reading->data[static::VALUE_FIELD];
    }

}
