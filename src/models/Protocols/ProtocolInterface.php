<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 11:19
 */

namespace Swoe\Models\Protocols;

use Swoe\Models\DocumentInterface;
use Swoe\Models\Readings\ReadingInterface;

interface ProtocolInterface extends DocumentInterface {
    public function readValue(ReadingInterface $reading);
}
