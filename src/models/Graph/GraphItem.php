<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 24/08/2015
 * Time: 17:02
 */

namespace Swoe\Models\Graph;


class GraphItem {
    public $id;
    public $location;
    public $value;
}
