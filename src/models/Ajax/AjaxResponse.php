<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 15/08/2015
 * Time: 13:40
 */

namespace Swoe\Models\Ajax;

class AjaxResponse {
    public $status = true;
    public $errors = [];
    public $data = null;
}
