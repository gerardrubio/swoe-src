<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 15/08/2015
 * Time: 11:00
 */

namespace Swoe\Models\Ajax;

class DataTablesResponse extends AjaxResponse {
    public $draw = 0;
    public $recordsTotal = 0;
    public $recordsFiltered = 0;
}
