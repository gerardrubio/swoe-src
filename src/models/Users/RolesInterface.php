<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 20:02
 */

namespace Swoe\Models\Users;

interface RolesInterface {
    /**
     * Roles
     */
    const VISITOR           = 1;    // 0x0001
    const REGISTERED_USER   = 2;    // 0x0010
    const ADMINISTRATOR     = 4;    // 0x0100

    /**
     * Filters
     */
    const FILTER_NONE       = 0;    // 0x0000
    const FILTER_SECURE     = 6;    // 0x0110
    const FILTER_ANY        = 15;   // 0x1111

    const AsList = [
        self::REGISTERED_USER   => 'User',
        self::ADMINISTRATOR     => 'Admin'
    ];
}
