<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Users;

use Swoe\Exceptions\DocumentNotFoundException;
use Swoe\Exceptions\UserNotFoundException;
use Swoe\Models\Document;
use Swoe\Models\StatusInterface;

class Users extends Document implements UserInterface {
    const PASSWORD_MIN_LENGTH = 8;
    public $email;
    public $password;
    public $token;
    public $name;
    public $surname;
    public $company;
    public $role = RolesInterface::VISITOR;

    public function registerAccess() {
        $this->status = StatusInterface::ACTIVE;
        $this->save();
    }

    public function isPendingPasswordConfirmation() {
        return $this->status === StatusInterface::NOT_CONFIRMED;
    }

    public function requirePasswordConfirmation($token) {
        $this->token = $token;
    }

    public function merge($raw) {
        $this->email = $raw->email;
        $this->password = $raw->password;
        $this->name = $raw->password;
        $this->surname = $raw->password;
        $this->company = $raw->password;
        $this->role = $raw->role;

        $this->sanitize();
    }

    public function sanitize() {
        $this->email = filter_var(trim($this->email), FILTER_SANITIZE_EMAIL);
        $this->password = filter_var($this->password, FILTER_SANITIZE_STRING);
        $this->name = filter_var(trim($this->name), FILTER_SANITIZE_STRING);
        $this->surname = filter_var(trim($this->surname), FILTER_SANITIZE_STRING);
        $this->company = filter_var(trim($this->company), FILTER_SANITIZE_STRING);
        $this->role = filter_var($this->role, FILTER_SANITIZE_NUMBER_INT);
    }

    public static function sanitized($raw) {
        $user = new Users();
        $user->email = $raw->email;
        $user->password = $raw->password;
        $user->name = $raw->name;
        $user->surname = $raw->surname;
        $user->company = $raw->company;
        $user->role = $raw->role;
        $user->sanitize();

        return $user;
    }

    public static function exists($email) {
        try {
            return static::findByEmail($email) !== null;
        } catch (DocumentNotFoundException $e){
            return false;
        }
    }

    public static function findActiveByEmail($email) {
        $users = static::find([
            'conditions' => [
                '$where' => 'this.status &= ' . StatusInterface::FILTER_CONFIRMED,
                'email' => $email
            ]
        ]);

        if (count($users) != 1)
            throw new UserNotFoundException;

        return $users[0];
    }

    public static function findByEmail($email) {
        return static::findOne('email', $email);
    }

    public static function findByToken($token) {
        return static::findOne('token', $token);
    }

    public function __get($property) {
        switch ($property) {
            case 'id':
                return $this->_id;
            case 'fullName':
                return $this->fullName();
        }

        throw new \Exception;
    }

    public function fullName() {
        return sprintf('%s %s (%s)', $this->name, $this->surname, $this->company);
    }

    public function __toString() {
        return $this->fullName();
    }
}
