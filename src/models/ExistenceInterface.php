<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 10:52
 */

namespace Swoe\Models;

interface ExistenceInterface {

    /**
     * @param mixed $value
     * @return bool
     */
    static function exists($value);
}
