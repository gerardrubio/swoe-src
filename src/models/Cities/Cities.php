<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Cities;

use Swoe\Models\Document;
use Swoe\Models\StatusInterface;

class Cities extends Document implements CityInterface {

    public $city;
    public $location;
    public $population;
    public $postalcode;
    public $province;

    /**
     * @param mixed $value
     * @return bool
     */
    static function exists($value)
    {
        // TODO: Implement exists() method.
    }

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw)
    {
        // TODO: Implement sanitized() method.
    }

    function sanitize()
    {
        // TODO: Implement sanitize() method.
    }
}
