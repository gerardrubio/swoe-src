<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 29/07/2015
 * Time: 19:59
 */

namespace Swoe\Models\Devices;

use Swoe\Models\Document;
use Swoe\Models\Graph\GraphItem;
use Swoe\Models\Readings\Readings;

class Devices extends Document implements DeviceInterface {
    public $name;
    public $description;
    public $ip;
    public $location = [
        'type' => 'Point',
        'coordinates' => [0, 0],
    ];
    public $protocol;
    public $owner;
    public $address;

    public static function exists($id) {
        return static::findOne('_id', $id) !== null;
    }

    public static function countOwnedBy($id) {
        return static::count([
            'conditions' => [
                'owner' => "$id"
            ]
        ]);
    }

    /**
     * @param $raw
     * @return mixed
     */
    static function sanitized($raw)
    {
        // TODO: Implement sanitized() method.
    }

    function sanitize()
    {
        $this->name = filter_var(trim($this->name), FILTER_SANITIZE_STRING);
        $this->description = filter_var(trim($this->description), FILTER_SANITIZE_STRING);
        $this->address = filter_var(trim($this->address), FILTER_SANITIZE_STRING);
        $this->ip = filter_var($this->ip, FILTER_SANITIZE_STRING);
        $this->owner = filter_var($this->owner, FILTER_SANITIZE_STRING);
        $this->protocol = filter_var($this->protocol, FILTER_SANITIZE_STRING);
    }

    /**
     * Get devices around specified coordinates, within a radius
     * @param $latitude
     * @param $longitude
     * @param $radius
     * @param array $returnFields fields to return
     * @param string $owner
     * @return array devices
     */
    public static function findAroundPoint($latitude, $longitude, $radius, $returnFields = [], $owner = '')
    {
        $conditions = [
            'location' => [
                '$geoWithin' => [
                    '$centerSphere' => [
                        [floatval($latitude), floatval($longitude)],
                        floatval($radius)
                    ]
                ]
            ]
        ];

        if (!empty($owner)) {
            $idField = '$id';
            $conditions['owner'] = $owner->$idField;
        }

        $devices = static::find([
            'conditions' => $conditions,
            'fields' => $returnFields
        ]);

        return $devices;
    }
}
