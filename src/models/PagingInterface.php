<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 10:52
 */

namespace Swoe\Models;

interface PagingInterface {
    const DEFAULT_PAGE = 0;
    const DEFAULT_PAGE_SIZE = 10;
    const MAX_PAGE_SIZE = 100;

    /**
     * @param int $page
     * @param int $size
     * @return array
     */
    static function page($page = self::DEFAULT_PAGE, $size = self::DEFAULT_PAGE_SIZE);

    /**
     * @param int $size
     * @return int
     */
    static function pages($size = self::DEFAULT_PAGE_SIZE);

    /**
     * @return int
     */
    static function total();
}
