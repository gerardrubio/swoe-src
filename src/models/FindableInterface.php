<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 08/08/2015
 * Time: 10:52
 */

namespace Swoe\Models;

interface FindableInterface {
    const ALL_DOCUMENTS = 0;
    const ONE_DOCUMENT = 1;
    const SKIP_NONE = 0;

    /**
     * @param array $fields
     * @param int $limit
     * @param int $skip
     * @param array $returnFields
     * @return array
     * @internal param string $field
     * @internal param mixed $value
     */
    static function findBy($fields = [], $limit = self::ALL_DOCUMENTS, $skip = self::SKIP_NONE, $returnFields = []);

    /**
     * @param string $field
     * @param mixed $value
     * @return mixed
     */
    static function findOne($field, $value);

    static function findRandom();
}
